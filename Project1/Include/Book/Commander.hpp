#ifndef BOOK_COMMANDER_HPP
#define BOOK_COMMANDER_HPP

#include "Entity.hpp"
#include "Command.hpp"
#include "ResourceIdentifiers.hpp"
#include "Projectile.hpp"
#include "TextNode.hpp"
#include "Animation.hpp"

#include "SFML/Graphics/Sprite.hpp"

class Commander : public Entity
{
public:
	enum class Type {
		Player1, Player2, Player3, Player4, Player5,
		Player6, Player7, Player8, Player9, Player10, TypeCount
	};

public:
	Commander(Type type, const TextureHolder& textures, const FontHolder& fonts);
	virtual unsigned int getCategory() const;
	virtual sf::FloatRect getBoundingRect() const;
	//int getLives();
	//void lifeLosted();
	virtual void remove();
	virtual bool isMarkedForRemoval() const;
	bool hasDied();
	//bool isAllied() const;
	float getMaxSpeed() const;

	int getIdentifier();
	void setIdentifier(int identifier);

	void increaseFireRate();
	void increaseSpread();

	void fire();
	void playLocalSound(CommandQueue& commands, SoundEffect::ID effect);

private:
	virtual void drawCurrent(sf::RenderTarget& target, sf::RenderStates states) const;
	virtual void updateCurrent(sf::Time dt, CommandQueue& commands);

	void checkPickupDrop(CommandQueue& commands);
	void checkProjectileLaunch(sf::Time dt, CommandQueue& commands);

	void createBullets(SceneNode& node, const TextureHolder& textures) const;
	void createProjectile(SceneNode& node, Projectile::Type type, float xOffset, float yOffset, const TextureHolder& textures) const;
	void createPickup(SceneNode& node, const TextureHolder& textures) const;
	void updateTexts();
	void updatePlayerAnimation();

private:
	Type mType;
	sf::Sprite mSprite;
	Animation mExplosion;
	Command mFireCommand;
	sf::Time mFireCountdown;
	bool mIsFiring;
	bool mShowExplosion;
	bool mPlayedExplosionSound;
	bool mSpawnedPickup;

	int mFireRateLevel;
	int mSpreadLevel;
	int mLives;

	Command mDropPickupCommand;
	float mTravelledDistance;
	std::size_t mDirectionIndex;
	TextNode* mHealthDisplay;
	TextNode* mLivesDisplay;

	int mIdentifier;
};

#endif // BOOK_COMMANDER_HPP