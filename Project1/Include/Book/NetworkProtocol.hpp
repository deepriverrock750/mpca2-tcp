#ifndef BOOK_NETWORKPROTOCOL_HPP
#define BOOK_NETWORKPROTOCOL_HPP

#include <SFML/Config.hpp>
#include <SFML/System/Vector2.hpp>


const unsigned short ServerPort = 6190;

namespace Server
{
	// Packets originated in the server
	enum PacketType
	{
		BroadcastMessage,	// format: [Int16:packetType] [string:message]
		SpawnSelf,			// format: [Int16:packetType]
		InitialState,
		BroadCastChatMessage,
		JoinLobby,
		PlayerEvent,
		PlayerRealtimeChange,
		PlayerConnect,
		PlayerDisconnect,
		AcceptCoopPartner,
		SpawnEnemy,
		SpawnPickup,
		StartGame,
		ChoseMap,
		UpdateClientState,
		MissionSuccess
	};
}

namespace Client
{
	// Packets originated in the client
	enum PacketType
	{
		PlayerEvent,
		SendChatMessage,
		ConnectedPlayers,
		Join,
		GameReady,
		MapVoting,
		PlayerRealtimeChange,
		RequestCoopPartner,
		PositionUpdate,
		GameEvent,
		Quit
	};
}

namespace PlayerActions
{
	enum Action
	{
		MoveLeft,
		MoveRight,
		MoveUp,
		MoveDown,
		Fire,
		LaunchMissile,
		ActionCount
	};
}

namespace GameActions
{
	enum Type
	{
		EnemyExplode,
	};

	struct Action
	{
		Action()
		{ // leave uninitialized
		}

		Action(Type type, sf::Vector2f position)
			: type(type)
			, position(position)
		{
		}

		Type			type;
		sf::Vector2f	position;
	};
}

#endif // BOOK_NETWORKPROTOCOL_HPP
