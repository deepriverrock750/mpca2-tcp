#ifndef BOOK_WALL_HPP
#define BOOK_WALL_HPP

#include "Entity.hpp"
#include "Command.hpp"
#include "ResourceIdentifiers.hpp"

#include <SFML/Graphics/RectangleShape.hpp>

class Wall : public Entity
{
public:

public:
	Wall(sf::Vector2f size, sf::Vector2f position);

	virtual unsigned int	getCategory() const;
	virtual sf::FloatRect	getBoundingRect() const;



protected:
	virtual void			drawCurrent(sf::RenderTarget& target, sf::RenderStates states) const;

private:
	sf::RectangleShape mRectangle;
};

#endif // BOOK_WALL_HPP