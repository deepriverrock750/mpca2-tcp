#include "../Include/Book/World.hpp"
#include "../Include/Book/Projectile.hpp"
#include "../Include/Book/Pickup.hpp"
#include "../Include/Book/Foreach.hpp"
#include "../Include/Book/TextNode.hpp"
#include "../Include/Book/ParticleNode.hpp"
#include "../Include/Book/SoundNode.hpp"
#include "../Include/Book/NetworkNode.hpp"
#include "../Include/Book/Utility.hpp"
#include "../Include/Book/Wall.hpp"

#include <SFML/Graphics/RenderTarget.hpp>

#include <algorithm>
#include <cmath>
#include <limits>

World::World(sf::RenderTarget& outputTarget, FontHolder& fonts, SoundPlayer& sounds, bool networked)
	: mTarget(outputTarget)
	, mSceneTexture()
	, mWorldView(outputTarget.getDefaultView())
	, mTextures()
	, mFonts(fonts)
	, mSounds(sounds)
	, mSceneGraph()
	, mSceneLayers()
	, mWorldBounds(0.f, 0.f, mWorldView.getSize().x, 768.f)
	, mSpawnPosition(mWorldView.getSize().x / 2.f, mWorldBounds.height - mWorldView.getSize().y / 2.f)
	, mScrollSpeed(0)
	, mScrollSpeedCompensation(0)
	, mPlayerAircrafts()
	, mPlayerCommanders()
	, mCollisionLeft(false)
	, mCollisionRight(false)
	, mCollisionDown(false)
	, mCollisionUp(false)
	, mEnemySpawnPoints()
	, mActiveEnemies()
	, mNetworkedWorld(networked)
	, mNetworkNode(nullptr)
	, mFinishSprite(nullptr)
{
	mSceneTexture.create(mTarget.getSize().x, mTarget.getSize().y);

	loadTextures();
	buildScene();

	// Prepare the view
	mWorldView.setCenter(mSpawnPosition);
}

void World::setWorldScrollCompensation(float compensation)
{
	mScrollSpeedCompensation = compensation;
}

void World::update(sf::Time dt)
{
	// Scroll the world, reset player velocity
	mWorldView.move(0.f, mScrollSpeed * dt.asSeconds() * mScrollSpeedCompensation);

	FOREACH(Aircraft* a, mPlayerAircrafts)
		a->setVelocity(0.f, 0.f);
	FOREACH(Commander* c, mPlayerCommanders)
		c->setVelocity(0.f, 0.f);

	// Setup commands to destroy entities, and guide missiles
	destroyEntitiesOutsideView();
	//guideMissiles();

	// Forward commands to scene graph, adapt velocity (scrolling, diagonal correction)
	while (!mCommandQueue.isEmpty())
		mSceneGraph.onCommand(mCommandQueue.pop(), dt);

	adaptPlayerVelocity();

	// Collision detection and response (may destroy entities)
	handleCollisions();

	// Remove aircrafts that were destroyed (World::removeWrecks() only destroys the entities, not the pointers in mPlayerAircraft)
	/*auto firstToRemove = std::remove_if(mPlayerAircrafts.begin(), mPlayerAircrafts.end(), std::mem_fn(&Aircraft::isMarkedForRemoval));
	mPlayerAircrafts.erase(firstToRemove, mPlayerAircrafts.end());*/

	// Remove players that were destroyed (World::removeWrecks() only destroys the entities, not the pointers in players)
	auto firstToRemove = std::remove_if(mPlayerCommanders.begin(), mPlayerCommanders.end(), std::mem_fn(&Commander::isMarkedForRemoval));
	mPlayerCommanders.erase(firstToRemove, mPlayerCommanders.end());

	// Remove all destroyed entities, create new ones
	mSceneGraph.removeWrecks();
	//spawnEnemies();

	// Regular update step, adapt position (correct if outside view)
	mSceneGraph.update(dt, mCommandQueue);
	adaptPlayerPosition();

	updateSounds();
}

void World::draw()
{
	if (PostEffect::isSupported())
	{
		mSceneTexture.clear();
		mSceneTexture.setView(mWorldView);
		mSceneTexture.draw(mSceneGraph);
		mSceneTexture.display();
		mBloomEffect.apply(mSceneTexture, mTarget);
	}
	else
	{
		mTarget.setView(mWorldView);
		mTarget.draw(mSceneGraph);
	}
}

CommandQueue& World::getCommandQueue()
{
	return mCommandQueue;
}

Aircraft* World::getAircraft(int identifier) const
{
	FOREACH(Aircraft* a, mPlayerAircrafts)
	{
		if (a->getIdentifier() == identifier)
			return a;
	}

	return nullptr;
}

void World::removeAircraft(int identifier)
{
	Aircraft* aircraft = getAircraft(identifier);
	if (aircraft)
	{
		aircraft->destroy();
		mPlayerAircrafts.erase(std::find(mPlayerAircrafts.begin(), mPlayerAircrafts.end(), aircraft));
	}
}

Aircraft* World::addAircraft(int identifier)
{
	std::unique_ptr<Aircraft> player1(new Aircraft(Aircraft::Eagle, mTextures, mFonts));
	player1->setPosition(mWorldView.getCenter());
	player1->setIdentifier(identifier);

	mPlayerAircrafts.push_back(player1.get());
	mSceneLayers[UpperAir]->attachChild(std::move(player1));
	return mPlayerAircrafts.back();
}

Commander* World::getCommander(int identifier) const
{
	FOREACH(Commander* c, mPlayerCommanders)
	{
		if (c->getIdentifier() == identifier)
			return c;
	}

	return nullptr;
}

void World::removeCommander(int identifier)
{
	Commander* commader = getCommander(identifier);
	if (commader)
	{
		commader->destroy();
		mPlayerCommanders.erase(std::find(mPlayerCommanders.begin(), mPlayerCommanders.end(), commader));
	}
}

Commander* World::addCommander(int identifier)
{
	Commander::Type playerType[10] = { Commander::Type::Player1, Commander::Type::Player2,Commander::Type::Player3, Commander::Type::Player4, Commander::Type::Player5, Commander::Type::Player6, Commander::Type::Player7, Commander::Type::Player8, Commander::Type::Player9, Commander::Type::Player10 };
	sf::Vector2f SpawnLocations[10] = { sf::Vector2f(70.f, 70.f), sf::Vector2f(410.f, 70.f), sf::Vector2f(1250.f, 70.f), sf::Vector2f(70.f, 365.f), sf::Vector2f(70.f, 700.f),
		sf::Vector2f(410.f, 700.f), sf::Vector2f(1250.f, 700.f), sf::Vector2f(1250.f, 365.f), sf::Vector2f(820.f, 70.f), sf::Vector2f(820.f, 700.f) };

	std::unique_ptr<Commander> player(new Commander(playerType[identifier], mTextures, mFonts));
	player->setPosition(SpawnLocations[identifier]);
	player->setIdentifier(identifier);

	mPlayerCommanders.push_back(player.get());
	mSceneLayers[UpperAir]->attachChild(std::move(player));
	return mPlayerCommanders.back();
}

void World::createPickup(sf::Vector2f position, Pickup::Type type)
{
	std::unique_ptr<Pickup> pickup(new Pickup(type, mTextures));
	pickup->setPosition(position);
	pickup->setVelocity(0.f, 1.f);
	mSceneLayers[UpperAir]->attachChild(std::move(pickup));
}

bool World::pollGameAction(GameActions::Action& out)
{
	return mNetworkNode->pollGameAction(out);
}

void World::setCurrentBattleFieldPosition(float lineY)
{
	mWorldView.setCenter(mWorldView.getCenter().x, lineY - mWorldView.getSize().y / 2);
	mSpawnPosition.y = mWorldBounds.height;
}

void World::setWorldHeight(float height)
{
	mWorldBounds.height = height;
}

bool World::hasAlivePlayer() const
{
	return mPlayerCommanders.size() > 0;
}

bool World::hasPlayerReachedEnd() const
{
	if (Aircraft* aircraft = getAircraft(1))
		return !mWorldBounds.contains(aircraft->getPosition());
	else
		return false;
}

void World::loadTextures()
{
	mTextures.load(Textures::Entities, "Media/Textures/Entities.png");
	mTextures.load(Textures::Players, "Media/Textures/Players.png");
	mTextures.load(Textures::Pickups, "Media/Textures/pickups.png");
	mTextures.load(Textures::Level, "Media/Textures/Level.png");
	mTextures.load(Textures::Jungle, "Media/Textures/Jungle.png");
	mTextures.load(Textures::Explosion, "Media/Textures/Explosion.png");
	mTextures.load(Textures::Particle, "Media/Textures/Particle.png");
	mTextures.load(Textures::FinishLine, "Media/Textures/FinishLine.png");
}

void World::adaptPlayerPosition()
{
	// Keep player's position inside the screen bounds, at least borderDistance units from the border
	sf::FloatRect viewBounds = getViewBounds();
	const float borderDistanceRight = 65.f;
	const float borderDistance = 40.f;

	/*FOREACH(Aircraft* aircraft, mPlayerAircrafts)
	{
	sf::Vector2f position = aircraft->getPosition();
	position.x = std::max(position.x, viewBounds.left + borderDistance);
	position.x = std::min(position.x, viewBounds.left + viewBounds.width - borderDistance);
	position.y = std::max(position.y, viewBounds.top + borderDistance);
	position.y = std::min(position.y, viewBounds.top + viewBounds.height - borderDistance);
	aircraft->setPosition(position);
	}*/

	FOREACH(Commander* commander, mPlayerCommanders)
	{
		sf::Vector2f position = commander->getPosition();
		position.x = std::max(position.x, viewBounds.left + borderDistanceRight);
		position.x = std::min(position.x, viewBounds.left + viewBounds.width - borderDistanceRight);
		position.y = std::max(position.y, viewBounds.top + borderDistanceRight);
		position.y = std::min(position.y, viewBounds.top + viewBounds.height - borderDistance);
		commander->setPosition(position);
	}
}

void World::adaptPlayerVelocity()
{
	//FOREACH(Aircraft* aircraft, mPlayerAircrafts)
	//{
	//	sf::Vector2f velocity = aircraft->getVelocity();

	//	// If moving diagonally, reduce velocity (to have always same velocity)
	//	if (velocity.x != 0.f && velocity.y != 0.f)
	//		aircraft->setVelocity(velocity / std::sqrt(2.f));

	//	// Add scrolling velocity
	//	aircraft->accelerate(0.f, mScrollSpeed);
	//}

	FOREACH(Commander* commander, mPlayerCommanders)
	{
		sf::Vector2f velocity = commander->getVelocity();

		// If moving diagonally, reduce velocity (to have always same velocity)
		if (velocity.x != 0.f && velocity.y != 0.f)
			commander->setVelocity(velocity / std::sqrt(2.f));

		// Add scrolling velocity
		commander->accelerate(0.f, mScrollSpeed);
	}
}

bool matchesCategories(SceneNode::Pair& colliders, Category::Type type1, Category::Type type2)
{
	unsigned int category1 = colliders.first->getCategory();
	unsigned int category2 = colliders.second->getCategory();

	// Make sure first pair entry has category type1 and second has type2
	if (type1 & category1 && type2 & category2)
	{
		return true;
	}
	else if (type1 & category2 && type2 & category1)
	{
		std::swap(colliders.first, colliders.second);
		return true;
	}
	else
	{
		return false;
	}
}

void World::handleCollisions()
{
	std::set<SceneNode::Pair> collisionPairs;
	mSceneGraph.checkSceneCollision(mSceneGraph, collisionPairs);

	FOREACH(SceneNode::Pair pair, collisionPairs)
	{
		if (matchesCategories(pair, Category::PlayerAircraft, Category::EnemyAircraft))
		{
			auto& player = static_cast<Aircraft&>(*pair.first);
			auto& enemy = static_cast<Aircraft&>(*pair.second);

			// Collision: Player damage = enemy's remaining HP
			player.damage(enemy.getHitpoints());
			enemy.destroy();
		}

		else if (matchesCategories(pair, Category::Player1, Category::Wall) ||
			matchesCategories(pair, Category::Player2, Category::Wall) ||
			matchesCategories(pair, Category::Player, Category::Wall))
		{
			auto& player = static_cast<Commander&>(*pair.first);
			auto& wall = static_cast<Wall&>(*pair.second);

			//sf::Vector2f playerLocation = player.getPosition();

			//try Collision Detect with sprite dir & velocity

			if (player.getVelocity().x > 0.f)
				mCollisionRight = true;
			if (player.getVelocity().x < 0.f)
				mCollisionLeft = true;
			if (player.getVelocity().y > 0.f)
				mCollisionDown = true;
			if (player.getVelocity().y < 0.f)
				mCollisionUp = true;

			if (mCollisionLeft)
			{
				//std::cout << "Collision Left! " << std::endl;
				//player.setPosition(playerLocation.x + 5.f, playerLocation.y);
				//player.setVelocityX(0);

				player.getVelocity().x < 0.f ? player.setVelocityX(0.f), player.setPosition(player.getPosition().x + 15.f, player.getPosition().y) : player.setVelocityX(5.f);

				/*player.setVelocityX(0);
				if (player.getVelocity().x < 0.f)
				player.setVelocityX(player.getMaxSpeed());
				else
				player.setVelocityX(0);*/
				mCollisionLeft = false;
			}
			if (mCollisionRight)
			{
				//std::cout << "Collision Right! " << std::endl;
				//player.setPosition(playerLocation.x - 5.f, playerLocation.y);
				//player.setVelocityX(0);

				player.getVelocity().x > 0.f ? player.setVelocityX(0.f), player.setPosition(player.getPosition().x - 15.f, player.getPosition().y) : player.setVelocityX(-5.f);

				mCollisionRight = false;
			}
			if (mCollisionDown)
			{
				//std::cout << "Collision Down! " << std::endl;
				//player.setPosition(playerLocation.x, playerLocation.y - 5.f);

				player.getVelocity().y > 0.f ? player.setVelocityY(0.f), player.setPosition(player.getPosition().x, player.getPosition().y - 15.f) : player.setVelocityY(-5.f);

				/*player.setVelocityY(0);*/
				mCollisionDown = false;
			}
			if (mCollisionUp)
			{
				//std::cout << "Collision Up! " << std::endl;
				//player.setPosition(playerLocation.x, playerLocation.y + 5.f);

				player.getVelocity().y < 0.f ? player.setVelocityY(0.f), player.setPosition(player.getPosition().x, player.getPosition().y + 15.f) : player.setVelocityY(5.f);

				/*player.setVelocityY(0);*/
				mCollisionUp = false;
			}
		}

		else if (matchesCategories(pair, Category::Player, Category::Pickup))
		{
			auto& player = static_cast<Commander&>(*pair.first);
			auto& pickup = static_cast<Pickup&>(*pair.second);

			// Apply pickup effect to player, destroy projectile
			pickup.apply(player);
			pickup.destroy();
			player.playLocalSound(mCommandQueue, SoundEffect::CollectPickup);
		}

		else if (matchesCategories(pair, Category::AlliedProjectile, Category::Wall) ||
			matchesCategories(pair, Category::EnemyProjectile, Category::Wall))
		{
			auto& bullet = static_cast<Projectile&>(*pair.first);
			auto& wall = static_cast<Wall&>(*pair.second);

			bullet.destroy();
		}

		else if (matchesCategories(pair, Category::Player, Category::AlliedProjectile)
			|| matchesCategories(pair, Category::Player, Category::EnemyProjectile))
		{
			auto& commander = static_cast<Commander&>(*pair.first);
			auto& projectile = static_cast<Projectile&>(*pair.second);

			// Apply projectile damage to aircraft, destroy projectile
			commander.damage(projectile.getDamage());
			projectile.destroy();
		}
	}
}

void World::updateSounds()
{
	sf::Vector2f listenerPosition;

	// 0 players (multiplayer mode, until server is connected) -> view center
	if (mPlayerAircrafts.empty())
	{
		listenerPosition = mWorldView.getCenter();
	}

	// 1 or more players -> mean position between all aircrafts
	else
	{
		FOREACH(Aircraft* aircraft, mPlayerAircrafts)
			listenerPosition += aircraft->getWorldPosition();

		listenerPosition /= static_cast<float>(mPlayerAircrafts.size());
	}

	// Set listener's position
	mSounds.setListenerPosition(listenerPosition);

	// Remove unused sounds
	mSounds.removeStoppedSounds();
}

void World::buildScene()
{
	// Initialize the different layers
	for (std::size_t i = 0; i < LayerCount; ++i)
	{
		Category::Type category = (i == LowerAir) ? Category::SceneAirLayer : Category::None;

		SceneNode::Ptr layer(new SceneNode(category));
		mSceneLayers[i] = layer.get();

		mSceneGraph.attachChild(std::move(layer));
	}

	sf::Texture& levelTexture = mTextures.get(Textures::Level);

	createWalls(sf::Vector2f(20, 100), sf::Vector2f(200, 150));
	createWalls(sf::Vector2f(200, 20), sf::Vector2f(200, 150));

	createWalls(sf::Vector2f(20, -100), sf::Vector2f(200, 618));
	createWalls(sf::Vector2f(200, 20), sf::Vector2f(200, 618));

	createWalls(sf::Vector2f(20, 100), sf::Vector2f(1146, 150));
	createWalls(sf::Vector2f(200, 20), sf::Vector2f(966, 150));

	createWalls(sf::Vector2f(20, 100), sf::Vector2f(1146, 518));
	createWalls(sf::Vector2f(200, 20), sf::Vector2f(966, 618));

	createWalls(sf::Vector2f(20, 100), sf::Vector2f((mWorldBounds.width / 2.f) + 100.f, (mWorldBounds.height / 2.f) + 80.f));
	createWalls(sf::Vector2f(150, 20), sf::Vector2f((mWorldBounds.width / 2.f) + 100.f, (mWorldBounds.height / 2.f) + 80.f));

	createWalls(sf::Vector2f(20, 100), sf::Vector2f((mWorldBounds.width / 2.f) - 120.f, (mWorldBounds.height / 2.f) + 80.f));
	createWalls(sf::Vector2f(-150, 20), sf::Vector2f((mWorldBounds.width / 2.f) - 100.f, (mWorldBounds.height / 2.f) + 80.f));

	createWalls(sf::Vector2f(20, -100), sf::Vector2f((mWorldBounds.width / 2.f) - 120.f, (mWorldBounds.height / 2.f) - 80.f));
	createWalls(sf::Vector2f(-150, 20), sf::Vector2f((mWorldBounds.width / 2.f) - 100.f, (mWorldBounds.height / 2.f) - 80.f));

	createWalls(sf::Vector2f(20, -100), sf::Vector2f((mWorldBounds.width / 2.f) + 100.f, (mWorldBounds.height / 2.f) - 80.f));
	createWalls(sf::Vector2f(150, 20), sf::Vector2f((mWorldBounds.width / 2.f) + 100.f, (mWorldBounds.height / 2.f) - 80.f));

	std::unique_ptr<SpriteNode> levelSprite(new SpriteNode(levelTexture));
	levelSprite->setPosition(mWorldBounds.left, mWorldBounds.top);
	mSceneLayers[Background]->attachChild(std::move(levelSprite));

	//// Prepare the tiled background
	//sf::Texture& jungleTexture = mTextures.get(Textures::Jungle);
	//jungleTexture.setRepeated(true);

	//float viewHeight = mWorldView.getSize().y;
	//sf::IntRect textureRect(mWorldBounds);
	//textureRect.height += static_cast<int>(viewHeight);

	//// Add the background sprite to the scene
	//std::unique_ptr<SpriteNode> jungleSprite(new SpriteNode(jungleTexture, textureRect));
	//jungleSprite->setPosition(mWorldBounds.left, mWorldBounds.top - viewHeight);
	//mSceneLayers[Background]->attachChild(std::move(jungleSprite));

	// Add the finish line to the scene
	/*sf::Texture& finishTexture = mTextures.get(Textures::FinishLine);
	std::unique_ptr<SpriteNode> finishSprite(new SpriteNode(finishTexture));
	finishSprite->setPosition(0.f, -76.f);
	mFinishSprite = finishSprite.get();
	mSceneLayers[Background]->attachChild(std::move(finishSprite));*/

	// Add particle node to the scene
	std::unique_ptr<ParticleNode> smokeNode(new ParticleNode(Particle::Smoke, mTextures));
	mSceneLayers[LowerAir]->attachChild(std::move(smokeNode));

	// Add propellant particle node to the scene
	std::unique_ptr<ParticleNode> propellantNode(new ParticleNode(Particle::Propellant, mTextures));
	mSceneLayers[LowerAir]->attachChild(std::move(propellantNode));

	// Add sound effect node
	std::unique_ptr<SoundNode> soundNode(new SoundNode(mSounds));
	mSceneGraph.attachChild(std::move(soundNode));

	// Add network node, if necessary
	if (mNetworkedWorld)
	{
		std::unique_ptr<NetworkNode> networkNode(new NetworkNode());
		mNetworkNode = networkNode.get();
		mSceneGraph.attachChild(std::move(networkNode));
	}

	// Add enemy aircraft
	//addEnemies();
}

void World::createWalls(sf::Vector2f size, sf::Vector2f position)
{
	std::unique_ptr<Wall> wall(new Wall(size, position));
	mSceneLayers[Layer::UpperAir]->attachChild(std::move(wall));
}

//void World::addEnemies()
//{
//	if (mNetworkedWorld)
//		return;
//
//	// Add enemies to the spawn point container
//	addEnemy(Aircraft::Raptor,    0.f,  500.f);
//	addEnemy(Aircraft::Raptor,    0.f, 1000.f);
//	addEnemy(Aircraft::Raptor, +100.f, 1150.f);
//	addEnemy(Aircraft::Raptor, -100.f, 1150.f);
//	addEnemy(Aircraft::Avenger,  70.f, 1500.f);
//	addEnemy(Aircraft::Avenger, -70.f, 1500.f);
//	addEnemy(Aircraft::Avenger, -70.f, 1710.f);
//	addEnemy(Aircraft::Avenger,  70.f, 1700.f);
//	addEnemy(Aircraft::Avenger,  30.f, 1850.f);
//	addEnemy(Aircraft::Raptor,  300.f, 2200.f);
//	addEnemy(Aircraft::Raptor, -300.f, 2200.f);
//	addEnemy(Aircraft::Raptor,    0.f, 2200.f);
//	addEnemy(Aircraft::Raptor,    0.f, 2500.f);
//	addEnemy(Aircraft::Avenger,-300.f, 2700.f);
//	addEnemy(Aircraft::Avenger,-300.f, 2700.f);
//	addEnemy(Aircraft::Raptor,    0.f, 3000.f);
//	addEnemy(Aircraft::Raptor,  250.f, 3250.f);
//	addEnemy(Aircraft::Raptor, -250.f, 3250.f);
//	addEnemy(Aircraft::Avenger,   0.f, 3500.f);
//	addEnemy(Aircraft::Avenger,   0.f, 3700.f);
//	addEnemy(Aircraft::Raptor,    0.f, 3800.f);
//	addEnemy(Aircraft::Avenger,   0.f, 4000.f);
//	addEnemy(Aircraft::Avenger,-200.f, 4200.f);
//	addEnemy(Aircraft::Raptor,  200.f, 4200.f);
//	addEnemy(Aircraft::Raptor,    0.f, 4400.f);
//
//	sortEnemies();
//}

void World::sortEnemies()
{
	// Sort all enemies according to their y value, such that lower enemies are checked first for spawning
	std::sort(mEnemySpawnPoints.begin(), mEnemySpawnPoints.end(), [](SpawnPoint lhs, SpawnPoint rhs)
	{
		return lhs.y < rhs.y;
	});
}

void World::addEnemy(Aircraft::Type type, float relX, float relY)
{
	SpawnPoint spawn(type, mSpawnPosition.x + relX, mSpawnPosition.y - relY);
	mEnemySpawnPoints.push_back(spawn);
}

//void World::spawnEnemies()
//{
//	// Spawn all enemies entering the view area (including distance) this frame
//	while (!mEnemySpawnPoints.empty()
//		&& mEnemySpawnPoints.back().y > getBattlefieldBounds().top)
//	{
//		SpawnPoint spawn = mEnemySpawnPoints.back();
//
//		std::unique_ptr<Aircraft> enemy(new Aircraft(spawn.type, mTextures, mFonts));
//		enemy->setPosition(spawn.x, spawn.y);
//		enemy->setRotation(180.f);
//		if (mNetworkedWorld) enemy->disablePickups();
//
//		mSceneLayers[UpperAir]->attachChild(std::move(enemy));
//
//		// Enemy is spawned, remove from the list to spawn
//		mEnemySpawnPoints.pop_back();
//	}
//}

void World::destroyEntitiesOutsideView()
{
	Command command;
	command.category = Category::Projectile | Category::EnemyAircraft;
	command.action = derivedAction<Entity>([this](Entity& e, sf::Time)
	{
		if (!getBattlefieldBounds().intersects(e.getBoundingRect()))
			e.remove();
	});

	mCommandQueue.push(command);
}

//void World::guideMissiles()
//{
//	// Setup command that stores all enemies in mActiveEnemies
//	Command enemyCollector;
//	enemyCollector.category = Category::EnemyAircraft;
//	enemyCollector.action = derivedAction<Aircraft>([this] (Aircraft& enemy, sf::Time)
//	{
//		if (!enemy.isDestroyed())
//			mActiveEnemies.push_back(&enemy);
//	});
//
//	// Setup command that guides all missiles to the enemy which is currently closest to the player
//	Command missileGuider;
//	missileGuider.category = Category::AlliedProjectile;
//	missileGuider.action = derivedAction<Projectile>([this] (Projectile& missile, sf::Time)
//	{
//		// Ignore unguided bullets
//		if (!missile.isGuided())
//			return;
//
//		float minDistance = std::numeric_limits<float>::max();
//		Aircraft* closestEnemy = nullptr;
//
//		// Find closest enemy
//		FOREACH(Aircraft* enemy, mActiveEnemies)
//		{
//			float enemyDistance = distance(missile, *enemy);
//
//			if (enemyDistance < minDistance)
//			{
//				closestEnemy = enemy;
//				minDistance = enemyDistance;
//			}
//		}
//
//		if (closestEnemy)
//			missile.guideTowards(closestEnemy->getWorldPosition());
//	});
//
//	// Push commands, reset active enemies
//	mCommandQueue.push(enemyCollector);
//	mCommandQueue.push(missileGuider);
//	mActiveEnemies.clear();
//}

sf::FloatRect World::getViewBounds() const
{
	return sf::FloatRect(mWorldView.getCenter() - mWorldView.getSize() / 2.f, mWorldView.getSize());
}

sf::FloatRect World::getBattlefieldBounds() const
{
	// Return view bounds + some area at top, where enemies spawn
	sf::FloatRect bounds = getViewBounds();
	bounds.top -= 100.f;
	bounds.height += 100.f;

	return bounds;
}