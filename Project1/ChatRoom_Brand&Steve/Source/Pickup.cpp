#include "../Include/Book/Pickup.hpp"
#include "../Include/Book/DataTables.hpp"
#include "../Include/Book/Category.hpp"
#include "../Include/Book/CommandQueue.hpp"
#include "../Include/Book/Utility.hpp"
#include "../Include/Book/ResourceHolder.hpp"

#include <SFML/Graphics/RenderTarget.hpp>


namespace
{
	const std::vector<PickupData> Table = initializePickupData();
}

Pickup::Pickup(Type type, const TextureHolder& textures)
: Entity(1)
, mType(type)
, mSprite(textures.get(Table[type].texture), Table[type].textureRect)
{
	centerOrigin(mSprite);
}

unsigned int Pickup::getCategory() const
{
	return Category::Pickup;
}

sf::FloatRect Pickup::getBoundingRect() const
{
	return getWorldTransform().transformRect(mSprite.getGlobalBounds());
}

void Pickup::apply(Commander& player) const
{
	Table[/*static_cast<int>*/(mType)].action(player);
}

void Pickup::drawCurrent(sf::RenderTarget& target, sf::RenderStates states) const
{
	target.draw(mSprite, states);
}

