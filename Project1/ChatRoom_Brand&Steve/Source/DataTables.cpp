#include "../Include/Book/DataTables.hpp"
#include "../Include/Book/Aircraft.hpp"
#include "../Include/Book/Commander.hpp"
#include "../Include/Book/Projectile.hpp"
#include "../Include/Book/Pickup.hpp"
#include "../Include/Book/Particle.hpp"


// For std::bind() placeholders _1, _2, ...
using namespace std::placeholders;

std::vector<AircraftData> initializeAircraftData()
{
	std::vector<AircraftData> data(Aircraft::TypeCount);

	data[Aircraft::Eagle].hitpoints = 100;
	data[Aircraft::Eagle].speed = 200.f;
	data[Aircraft::Eagle].fireInterval = sf::seconds(1);
	data[Aircraft::Eagle].texture = Textures::Entities;
	data[Aircraft::Eagle].textureRect = sf::IntRect(0, 0, 48, 64);
	data[Aircraft::Eagle].hasRollAnimation = true;

	data[Aircraft::Raptor].hitpoints = 20;
	data[Aircraft::Raptor].speed = 80.f;
	data[Aircraft::Raptor].texture = Textures::Entities;
	data[Aircraft::Raptor].textureRect = sf::IntRect(144, 0, 84, 64);
	data[Aircraft::Raptor].directions.push_back(Direction(+45.f, 80.f));
	data[Aircraft::Raptor].directions.push_back(Direction(-45.f, 160.f));
	data[Aircraft::Raptor].directions.push_back(Direction(+45.f, 80.f));
	data[Aircraft::Raptor].fireInterval = sf::Time::Zero;
	data[Aircraft::Raptor].hasRollAnimation = false;

	data[Aircraft::Avenger].hitpoints = 40;
	data[Aircraft::Avenger].speed = 50.f;
	data[Aircraft::Avenger].texture = Textures::Entities;
	data[Aircraft::Avenger].textureRect = sf::IntRect(228, 0, 60, 59);
	data[Aircraft::Avenger].directions.push_back(Direction(+45.f, 50.f));
	data[Aircraft::Avenger].directions.push_back(Direction(0.f, 50.f));
	data[Aircraft::Avenger].directions.push_back(Direction(-45.f, 100.f));
	data[Aircraft::Avenger].directions.push_back(Direction(0.f, 50.f));
	data[Aircraft::Avenger].directions.push_back(Direction(+45.f, 50.f));
	data[Aircraft::Avenger].fireInterval = sf::seconds(2);
	data[Aircraft::Avenger].hasRollAnimation = false;

	return data;
}

std::vector<ProjectileData> initializeProjectileData()
{
	std::vector<ProjectileData> data(Projectile::TypeCount);

	data[Projectile::Bullet].damage = 20;
	data[Projectile::Bullet].speed = 600.f;
	data[Projectile::Bullet].texture = Textures::Pickups;
	data[Projectile::Bullet].textureRect = sf::IntRect(120, 0, 3, 14);

	/*data[Projectile::AlliedBullet].damage = 20;
	data[Projectile::AlliedBullet].speed = 600.f;
	data[Projectile::AlliedBullet].texture = Textures::Entities;
	data[Projectile::AlliedBullet].textureRect = sf::IntRect(175, 64, 3, 14);

	data[Projectile::EnemyBullet].damage = 10;
	data[Projectile::EnemyBullet].speed = 300.f;
	data[Projectile::EnemyBullet].texture = Textures::Entities;
	data[Projectile::EnemyBullet].textureRect = sf::IntRect(178, 64, 3, 14);

	data[Projectile::Missile].damage = 200;
	data[Projectile::Missile].speed = 150.f;
	data[Projectile::Missile].texture = Textures::Entities;
	data[Projectile::Missile].textureRect = sf::IntRect(160, 64, 15, 32);*/

	return data;
}
std::vector<PlayerData> initializePlayerData()
{
	std::vector<PlayerData> data(static_cast<int>(Commander::Type::TypeCount));
	data[static_cast<int>(Commander::Type::Player1)].hitpoints = 100;
	data[static_cast<int>(Commander::Type::Player1)].speed = 300.f;
	data[static_cast<int>(Commander::Type::Player1)].fireInterval = sf::seconds(2);
	data[static_cast<int>(Commander::Type::Player1)].texture = Textures::Players;
	//data[static_cast<int>(Commander::Type::Player1)].texture = Textures::Entities;
	data[static_cast<int>(Commander::Type::Player1)].textureRect = sf::IntRect(0, 0, 86, 50);

	data[static_cast<int>(Commander::Type::Player2)].hitpoints = 100;
	data[static_cast<int>(Commander::Type::Player2)].speed = 300.f;
	data[static_cast<int>(Commander::Type::Player2)].fireInterval = sf::seconds(2);
	data[static_cast<int>(Commander::Type::Player2)].texture = Textures::Players;
	//data[static_cast<int>(Commander::Type::Player2)].texture = Textures::Entities;
	data[static_cast<int>(Commander::Type::Player2)].textureRect = sf::IntRect(0, 50, 86, 50);

	data[static_cast<int>(Commander::Type::Player3)].hitpoints = 100;
	data[static_cast<int>(Commander::Type::Player3)].speed = 300.f;
	data[static_cast<int>(Commander::Type::Player3)].fireInterval = sf::seconds(2);
	data[static_cast<int>(Commander::Type::Player3)].texture = Textures::Players;
	data[static_cast<int>(Commander::Type::Player3)].textureRect = sf::IntRect(0, 100, 86, 50);

	data[static_cast<int>(Commander::Type::Player4)].hitpoints = 100;
	data[static_cast<int>(Commander::Type::Player4)].speed = 300.f;
	data[static_cast<int>(Commander::Type::Player4)].fireInterval = sf::seconds(2);
	data[static_cast<int>(Commander::Type::Player4)].texture = Textures::Players;
	data[static_cast<int>(Commander::Type::Player4)].textureRect = sf::IntRect(0, 150, 86, 50);

	data[static_cast<int>(Commander::Type::Player5)].hitpoints = 100;
	data[static_cast<int>(Commander::Type::Player5)].speed = 300.f;
	data[static_cast<int>(Commander::Type::Player5)].fireInterval = sf::seconds(2);
	data[static_cast<int>(Commander::Type::Player5)].texture = Textures::Players;
	data[static_cast<int>(Commander::Type::Player5)].textureRect = sf::IntRect(0, 200, 86, 50);

	data[static_cast<int>(Commander::Type::Player6)].hitpoints = 100;
	data[static_cast<int>(Commander::Type::Player6)].speed = 300.f;
	data[static_cast<int>(Commander::Type::Player6)].fireInterval = sf::seconds(2);
	data[static_cast<int>(Commander::Type::Player6)].texture = Textures::Players;
	data[static_cast<int>(Commander::Type::Player6)].textureRect = sf::IntRect(86, 0, 86, 50);

	data[static_cast<int>(Commander::Type::Player7)].hitpoints = 100;
	data[static_cast<int>(Commander::Type::Player7)].speed = 300.f;
	data[static_cast<int>(Commander::Type::Player7)].fireInterval = sf::seconds(2);
	data[static_cast<int>(Commander::Type::Player7)].texture = Textures::Players;
	data[static_cast<int>(Commander::Type::Player7)].textureRect = sf::IntRect(86, 50, 86, 50);

	data[static_cast<int>(Commander::Type::Player8)].hitpoints = 100;
	data[static_cast<int>(Commander::Type::Player8)].speed = 300.f;
	data[static_cast<int>(Commander::Type::Player8)].fireInterval = sf::seconds(2);
	data[static_cast<int>(Commander::Type::Player8)].texture = Textures::Players;
	data[static_cast<int>(Commander::Type::Player8)].textureRect = sf::IntRect(86, 100, 86, 50);

	data[static_cast<int>(Commander::Type::Player9)].hitpoints = 100;
	data[static_cast<int>(Commander::Type::Player9)].speed = 300.f;
	data[static_cast<int>(Commander::Type::Player9)].fireInterval = sf::seconds(2);
	data[static_cast<int>(Commander::Type::Player9)].texture = Textures::Players;
	data[static_cast<int>(Commander::Type::Player9)].textureRect = sf::IntRect(86, 150, 86, 50);

	data[static_cast<int>(Commander::Type::Player10)].hitpoints = 100;
	data[static_cast<int>(Commander::Type::Player10)].speed = 300.f;
	data[static_cast<int>(Commander::Type::Player10)].fireInterval = sf::seconds(2);
	data[static_cast<int>(Commander::Type::Player10)].texture = Textures::Players;
	data[static_cast<int>(Commander::Type::Player10)].textureRect = sf::IntRect(86, 200, 86, 50);

	return data;
}

std::vector<PickupData> initializePickupData()
{
	std::vector<PickupData> data(Pickup::TypeCount);

	data[Pickup::HealthRefill].texture = Textures::Pickups;
	data[Pickup::HealthRefill].textureRect = sf::IntRect(0, 0, 40, 40);
	data[Pickup::HealthRefill].action = [](Commander& a) { a.repair(40); };

	/*data[Pickup::MissileRefill].texture = Textures::Entities;
	data[Pickup::MissileRefill].textureRect = sf::IntRect(40, 64, 40, 40);
	data[Pickup::MissileRefill].action = std::bind(&Commander::collectMissiles, _1, 3);*/

	data[Pickup::FireSpread].texture = Textures::Pickups;
	data[Pickup::FireSpread].textureRect = sf::IntRect(40, 0, 40, 40);
	data[Pickup::FireSpread].action = std::bind(&Commander::increaseSpread, _1);

	data[Pickup::FireRate].texture = Textures::Pickups;
	data[Pickup::FireRate].textureRect = sf::IntRect(80, 0, 40, 40);
	data[Pickup::FireRate].action = std::bind(&Commander::increaseFireRate, _1);

	return data;
}

std::vector<ParticleData> initializeParticleData()
{
	std::vector<ParticleData> data(Particle::ParticleCount);

	data[Particle::Propellant].color = sf::Color(255, 255, 50);
	data[Particle::Propellant].lifetime = sf::seconds(0.6f);

	data[Particle::Smoke].color = sf::Color(50, 50, 50);
	data[Particle::Smoke].lifetime = sf::seconds(4.f);

	return data;
}
