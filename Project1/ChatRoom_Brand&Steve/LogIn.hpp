#include "Include/Book/State.hpp"

#include <SFML/Graphics/Sprite.hpp>
#include <SFML/Graphics/Text.hpp>


class LogIn : public State
{
public:
	LogIn(StateStack& stack, Context context, bool isHost);

	virtual void				draw();
	virtual bool				update(sf::Time dt);
	virtual bool				handleEvent(const sf::Event& event);

private:
	bool						checkIfFileExists();
	std::string					getNameFromFile();
	void						writeNameToFile();

private:
	sf::RenderWindow&			mWindow;
	bool						mHost;
	bool						mHasName;

	sf::Text					mNameText;
	sf::Text					mInputText;
	std::string					mName;
};
