#include "LogIn.hpp"

#include "Include/Book/MultiplayerGameState.hpp"
#include "Include/Book/MusicPlayer.hpp"
#include "Include/Book/Foreach.hpp"
#include "Include/Book/Utility.hpp"

#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/Network/IpAddress.hpp>

#include <fstream>

LogIn::LogIn(StateStack & stack, Context context, bool isHost)
	: State(stack, context)
	, mWindow(*context.window)
	, mHost(isHost)
{
	mWindow.clear(sf::Color::Black);

	mNameText.setFont(context.fonts->get(Fonts::Main));
	mNameText.setString("Input your Name:");
	mNameText.setCharacterSize(35);
	centerOrigin(mNameText);
	mNameText.setPosition(mWindow.getSize().x / 2.f, ((mWindow.getSize().y / 2.f) - 40));

	mInputText.setFont(context.fonts->get(Fonts::Main));
	mInputText.setString("");
	mInputText.setCharacterSize(35);
	centerOrigin(mInputText);
	mInputText.setPosition(mWindow.getSize().x / 2.f, mWindow.getSize().y / 2.);
}

void LogIn::draw()
{
	if (!checkIfFileExists())
	{
		mWindow.draw(mNameText);
		mWindow.draw(mInputText);
	}
}

bool LogIn::update(sf::Time dt)
{
	if (mHasName)
	{
		if (mHost)
		{
			requestStackPop();
			requestStackPush(States::HostGame);
		}
		else
		{
			requestStackPop();
			requestStackPush(States::JoinGame);
		}
	}
	return false;
}

bool LogIn::handleEvent(const sf::Event & event)
{
	if (event.type == sf::Event::KeyPressed)
	{
		if (event.key.code == sf::Keyboard::Return)
		{
			// Go to Online State
			if (mName.length() > 0)
			{
				writeNameToFile();
			}

		}
		else if (event.key.code == sf::Keyboard::BackSpace)
		{
			if (mName.length() > 0)
			{
				mName = mName.substr(0, mName.size() - 1);
			}

		}
		// Escape pressed, trigger the pause screen
		else if (event.key.code == sf::Keyboard::Escape)
		{
		}
	}
	else if (event.type == sf::Event::TextEntered)
	{
		if (32 < event.text.unicode < 128)
		{
			if (event.text.unicode != '\b')
			{
				if (mName.length() <= 15)
				{
					mName += static_cast<char>(event.text.unicode);
				}
			}
			mInputText.setString(mName);
			centerOrigin(mInputText);

		}
	}

	return false;
}

bool LogIn::checkIfFileExists()
{

	std::ifstream inputFile("profile.txt");
	std::string name;
	if (inputFile >> name)
	{
		mHasName = true;
		return true;
	}
	return false;
}

void LogIn::writeNameToFile()
{
	// If open/read failed, create new file
	std::ofstream outputFile("profile.txt");
	std::string name = mName;
	outputFile << mName;

	mHasName = true;
}
