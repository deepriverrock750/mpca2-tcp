#include "../Include/Book/GameServer.hpp"
#include "../Include/Book/NetworkProtocol.hpp"
#include "../Include/Book/Foreach.hpp"
#include "../Include/Book/Utility.hpp"
#include "../Include/Book/Pickup.hpp"
#include "../Include/Book/Aircraft.hpp"
#include "../Include/Book/Commander.hpp"

#include <SFML/Network/Packet.hpp>

#include <iostream>

GameServer::RemotePeer::RemotePeer()
	: ready(false)
	, timedOut(false)
{
	socket.setBlocking(false);
}

GameServer::GameServer(sf::Vector2f battlefieldSize)
	: mThread(&GameServer::executionThread, this)
	, mListeningState(false)
	, mClientTimeoutTime(sf::seconds(3.f))
	, mMaxConnectedPlayers(10)
	, mConnectedPlayers(0)
	, mWorldHeight(786.f)
	, mBattleFieldRect(0.f, mWorldHeight - battlefieldSize.y, battlefieldSize.x, battlefieldSize.y)
	, mBattleFieldScrollSpeed(0.f)
	//, mAircraftCount(0)
	, mCommanderCount(0)
	, mPeers(1)
	//, mAircraftIdentifierCounter(1)
	, mCommanderIdentifierCounter(1)
	, mWaitingThreadEnd(false)
	, mLastSpawnTime(sf::Time::Zero)
	, mTimeForNextSpawn(sf::seconds(5.f))
{
	mListenerSocket.setBlocking(false);
	mPeers[0].reset(new RemotePeer());
	mThread.launch();
}

GameServer::~GameServer()
{
	mWaitingThreadEnd = true;
	mThread.wait();
}

void GameServer::notifyPlayerRealtimeChange(sf::Int32 commanderIdentifier, sf::Int32 action, bool actionEnabled)
{
	for (std::size_t i = 0; i < mConnectedPlayers; ++i)
	{
		if (mPeers[i]->ready)
		{
			sf::Packet packet;
			packet << static_cast<sf::Int32>(Server::PlayerRealtimeChange);
			packet << commanderIdentifier;
			packet << action;
			packet << actionEnabled;

			mPeers[i]->socket.send(packet);
		}
	}
}

void GameServer::notifyPlayerEvent(sf::Int32 commanderIdentifier, sf::Int32 action)
{
	for (std::size_t i = 0; i < mConnectedPlayers; ++i)
	{
		if (mPeers[i]->ready)
		{
			sf::Packet packet;
			packet << static_cast<sf::Int32>(Server::PlayerEvent);
			packet << commanderIdentifier;
			packet << action;

			mPeers[i]->socket.send(packet);
		}
	}
}

void GameServer::notifyPlayerSpawn(sf::Int32 commanderIdentifier)
{
	for (std::size_t i = 0; i < mConnectedPlayers; ++i)
	{
		if (mPeers[i]->ready)
		{
			sf::Packet packet;
			packet << static_cast<sf::Int32>(Server::PlayerConnect);
			packet << commanderIdentifier << mCommanderInfo[commanderIdentifier].position.x << mCommanderInfo[commanderIdentifier].position.y;
			mPeers[i]->socket.send(packet);
		}
	}
}

void GameServer::setListening(bool enable)
{
	// Check if it isn't already listening
	if (enable)
	{
		if (!mListeningState)
			mListeningState = (mListenerSocket.listen(ServerPort) == sf::TcpListener::Done);
	}
	else
	{
		mListenerSocket.close();
		mListeningState = false;
	}
}

void GameServer::executionThread()
{
	setListening(true);

	sf::Time stepInterval = sf::seconds(1.f / 60.f);
	sf::Time stepTime = sf::Time::Zero;
	sf::Time tickInterval = sf::seconds(1.f / 20.f);
	sf::Time tickTime = sf::Time::Zero;
	sf::Clock stepClock, tickClock;

	while (!mWaitingThreadEnd)
	{
		handleIncomingPackets();
		handleIncomingConnections();

		stepTime += stepClock.getElapsedTime();
		stepClock.restart();

		tickTime += tickClock.getElapsedTime();
		tickClock.restart();

		// Fixed update step
		while (stepTime >= stepInterval)
		{
			mBattleFieldRect.top += mBattleFieldScrollSpeed * stepInterval.asSeconds();
			stepTime -= stepInterval;
		}

		// Fixed tick step
		while (tickTime >= tickInterval)
		{
			tick();
			tickTime -= tickInterval;
		}

		// Sleep to prevent server from consuming 100% CPU
		sf::sleep(sf::milliseconds(100));
	}
}

void GameServer::tick()
{
	updateClientState();
	// Check For last player alive here I suppose
	//Check for mission success = all planes with position.y < offset
	bool allPlayersDead = true;
	//FOREACH(auto pair, mCommanderInfo)
	//{
	//	// As long as one player has not crossed the finish line yet, set variable to false
	//	if (pair.second.hitpoints > 0.f)
	//		allPLayersDead = false;
	//}
	if (mCommanderInfo.size() > 1)
	{
		allPlayersDead = false;
	}
	std::cout << mCommanderInfo.size() << std::endl;
	if (allPlayersDead)
	{
		sf::Packet missionSuccessPacket;
		missionSuccessPacket << static_cast<sf::Int32>(Server::MissionSuccess);
		sendToAll(missionSuccessPacket);
	}

	 //Remove IDs of aircraft that have been destroyed (relevant if a client has two, and loses one)
	/*for (auto itr = mAircraftInfo.begin(); itr != mAircraftInfo.end(); )
	{
		if (itr->second.hitpoints <= 0)
			mAircraftInfo.erase(itr++);
		else
			++itr;
	}*/

	// Remove IDs of Commanders that have been destroyed (relevant if a client has two, and loses one)
	for (auto itr = mCommanderInfo.begin(); itr != mCommanderInfo.end(); )
	{
		if (itr->second.hitpoints <= 0)
			mCommanderInfo.erase(itr++);
		else
			++itr;
	}
}

sf::Time GameServer::now() const
{
	return mClock.getElapsedTime();
}

void GameServer::handleIncomingPackets()
{
	bool detectedTimeout = false;

	FOREACH(PeerPtr& peer, mPeers)
	{
		if (peer->ready)
		{
			sf::Packet packet;
			while (peer->socket.receive(packet) == sf::Socket::Done)
			{
				// Interpret packet and react to it
				handleIncomingPacket(packet, *peer, detectedTimeout);

				// Packet was indeed received, update the ping timer
				peer->lastPacketTime = now();
				packet.clear();
			}

			if (now() >= peer->lastPacketTime + mClientTimeoutTime)
			{
				peer->timedOut = true;
				detectedTimeout = true;
			}
		}
	}

	if (detectedTimeout)
		handleDisconnections();
}

void GameServer::handleIncomingPacket(sf::Packet& packet, RemotePeer& receivingPeer, bool& detectedTimeout)
{
	sf::Int32 packetType;
	packet >> packetType;

	switch (packetType)
	{
	case Client::Quit:
	{
		receivingPeer.timedOut = true;
		detectedTimeout = true;
	} break;

	case Client::PlayerEvent:
	{
		sf::Int32 commanderIdentifier;
		sf::Int32 action;
		packet >> commanderIdentifier >> action;

		notifyPlayerEvent(commanderIdentifier, action);
	} break;

	case Client::PlayerRealtimeChange:
	{
		sf::Int32 commanderIdentifier;
		sf::Int32 action;
		bool actionEnabled;
		packet >> commanderIdentifier >> action >> actionEnabled;
		mCommanderInfo[commanderIdentifier].realtimeActions[action] = actionEnabled;

		notifyPlayerRealtimeChange(commanderIdentifier, action, actionEnabled);
	} break;

	case Client::PositionUpdate:
	{
		sf::Int32 numCommanders;
		packet >> numCommanders;

		for (sf::Int32 i = 0; i < numCommanders; ++i)
		{
			sf::Int32 commanderIdentifier;
			sf::Int32 commanderHitpoints;
			sf::Vector2f commanderPosition;
			packet >> commanderIdentifier >> commanderPosition.x >> commanderPosition.y >> commanderHitpoints;
			mCommanderInfo[commanderIdentifier].position = commanderPosition;
			mCommanderInfo[commanderIdentifier].hitpoints = commanderHitpoints;
		} break;

	case Client::GameEvent:
	{
		sf::Int32 action;
		float x;
		float y;

		packet >> action;
		packet >> x;
		packet >> y;

		// Enemy explodes: With certain probability, drop pickup
		// To avoid multiple messages spawning multiple pickups, only listen to first peer (host)
		if (action == GameActions::EnemyExplode && randomInt(2) == 0 && &receivingPeer == mPeers[0].get())
		{
			sf::Packet packet;
			packet << static_cast<sf::Int32>(Server::SpawnPickup);
			packet << static_cast<sf::Int32>(randomInt(Pickup::TypeCount));
			packet << x;
			packet << y;

			sendToAll(packet);
		}
	}break;

	// Additonal Stuff
	case Client::Join:
	{
		std::string name;
		packet >> name;

		sf::Packet chatPacket;
		chatPacket << static_cast<sf::Int32>(Server::JoinLobby) << name;

		sendToAll(chatPacket);
	}break;

	case Client::ConnectedPlayers:
	{
	}break;

	case Client::SendChatMessage:
	{
		std::string chatMessage;
		packet >> chatMessage;

		sf::Packet chatPacket;
		chatPacket << static_cast<sf::Int32>(Server::BroadCastChatMessage) << chatMessage;

		sendToAll(chatPacket);
	}break;
	case Client::GameReady:
	{
		sf::Packet packet;
		packet << static_cast<sf::Int32>(Server::StartGame);

		sendToAll(packet);
	}break;
	case Client::MapVoting:
	{
		sf::Int32 map1, map2, map3;

		packet >> map1 >> map2 >> map3;

		sf::Packet mapPacket;
		mapPacket << static_cast<sf::Int32>(Server::ChoseMap) << map1 << map2 << map3;

		sendToAll(mapPacket);
	}break;
	}
	}
}

void GameServer::updateClientState()
{
	sf::Packet updateClientStatePacket;
	updateClientStatePacket << static_cast<sf::Int32>(Server::UpdateClientState);
	updateClientStatePacket << static_cast<float>(mBattleFieldRect.top + mBattleFieldRect.height);
	updateClientStatePacket << static_cast<sf::Int32>(mCommanderInfo.size());

	FOREACH(auto commander, mCommanderInfo)
		updateClientStatePacket << commander.first << commander.second.position.x << commander.second.position.y;

	sendToAll(updateClientStatePacket);
}

void GameServer::handleIncomingConnections()
{
	if (!mListeningState)
		return;

	if (mListenerSocket.accept(mPeers[mConnectedPlayers]->socket) == sf::TcpListener::Done)
	{
		// order the new client to spawn its own player ( player 1 )
		//Commander::Type playerType[10] = { Commander::Type::Player1, Commander::Type::Player2,Commander::Type::Player3, Commander::Type::Player4, Commander::Type::Player5, Commander::Type::Player6, Commander::Type::Player7, Commander::Type::Player8, Commander::Type::Player9, Commander::Type::Player10 };
		sf::Vector2f SpawnLocations[10] = { sf::Vector2f(70.f, 70.f), sf::Vector2f(410.f, 70.f), sf::Vector2f(1250.f, 70.f), sf::Vector2f(70.f, 365.f), sf::Vector2f(70.f, 700.f),
			sf::Vector2f(410.f, 700.f), sf::Vector2f(1250.f, 700.f), sf::Vector2f(1250.f, 365.f), sf::Vector2f(820.f, 70.f), sf::Vector2f(820.f, 700.f) };


		mCommanderInfo[mCommanderIdentifierCounter].position = SpawnLocations[mCommanderIdentifierCounter-1];
		mCommanderInfo[mCommanderIdentifierCounter].hitpoints = 100;

		sf::Packet packet;
		packet << static_cast<sf::Int32>(Server::SpawnSelf);
		packet << mCommanderIdentifierCounter;
		packet << mCommanderInfo[mCommanderIdentifierCounter].position.x;
		packet << mCommanderInfo[mCommanderIdentifierCounter].position.y;
		//packet << mConnectedPlayers + 1;

		mPeers[mConnectedPlayers]->commanderIdentifiers.push_back(mCommanderIdentifierCounter);

		broadcastMessage("New player!");
		informWorldState(mPeers[mConnectedPlayers]->socket);
		notifyPlayerSpawn(mCommanderIdentifierCounter++);

		mPeers[mConnectedPlayers]->socket.send(packet);
		mPeers[mConnectedPlayers]->ready = true;
		mPeers[mConnectedPlayers]->lastPacketTime = now(); // prevent initial timeouts
		mCommanderCount++;
		mConnectedPlayers++;

		if (mConnectedPlayers >= mMaxConnectedPlayers)
			setListening(false);
		else // Add a new waiting peer
			mPeers.push_back(PeerPtr(new RemotePeer()));
	}
}

void GameServer::handleDisconnections()
{
	for (auto itr = mPeers.begin(); itr != mPeers.end(); )
	{
		if ((*itr)->timedOut)
		{
			// Inform everyone of the disconnection, erase
			FOREACH(sf::Int32 identifier, (*itr)->commanderIdentifiers)
			{
				sendToAll(sf::Packet() << static_cast<sf::Int32>(Server::PlayerDisconnect) << identifier);

				mCommanderInfo.erase(identifier);
			}

			mConnectedPlayers--;
			mCommanderCount -= (*itr)->commanderIdentifiers.size();

			itr = mPeers.erase(itr);

			// Go back to a listening state if needed
			if (mConnectedPlayers < mMaxConnectedPlayers)
			{
				mPeers.push_back(PeerPtr(new RemotePeer()));
				setListening(true);
			}

			broadcastMessage("An ally has disconnected.");
		}
		else
		{
			++itr;
		}
	}
}

// Tell the newly connected peer about how the world is currently
void GameServer::informWorldState(sf::TcpSocket& socket)
{
	sf::Packet packet;
	packet << static_cast<sf::Int32>(Server::InitialState);
	packet << mWorldHeight << mBattleFieldRect.top + mBattleFieldRect.height;
	packet << static_cast<sf::Int32>(mCommanderCount);

	for (std::size_t i = 0; i < mConnectedPlayers; ++i)
	{
		if (mPeers[i]->ready)
		{
			FOREACH(sf::Int32 identifier, mPeers[i]->commanderIdentifiers)
				packet << identifier << mCommanderInfo[identifier].position.x << mCommanderInfo[identifier].position.y << mCommanderInfo[identifier].hitpoints;
		}
	}

	socket.send(packet);
}

void GameServer::broadcastMessage(const std::string& message)
{
	for (std::size_t i = 0; i < mConnectedPlayers; ++i)
	{
		if (mPeers[i]->ready)
		{
			sf::Packet packet;
			packet << static_cast<sf::Int32>(Server::BroadcastMessage);
			packet << message;

			mPeers[i]->socket.send(packet);
		}
	}
}

void GameServer::sendToAll(sf::Packet& packet)
{
	FOREACH(PeerPtr& peer, mPeers)
	{
		if (peer->ready)
			peer->socket.send(packet);
	}
}