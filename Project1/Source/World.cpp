#include "../Include/Book/World.hpp"
#include "../Include/Book/Projectile.hpp"
#include "../Include/Book/Pickup.hpp"
#include "../Include/Book/Foreach.hpp"
#include "../Include/Book/TextNode.hpp"
#include "../Include/Book/ParticleNode.hpp"
#include "../Include/Book/SoundNode.hpp"
#include "../Include/Book/NetworkNode.hpp"
#include "../Include/Book/Utility.hpp"
#include "../Include/Book/Wall.hpp"

#include <SFML/Graphics/RenderTarget.hpp>

#include <algorithm>
#include <cmath>
#include <limits>
#include <iostream>

World::World(sf::RenderTarget& outputTarget, FontHolder& fonts, SoundPlayer& sounds, bool networked)
	: mTarget(outputTarget)
	, mSceneTexture()
	, mWorldView(outputTarget.getDefaultView())
	, mTextures()
	, mFonts(fonts)
	, mSounds(sounds)
	, mSceneGraph()
	, mSceneLayers()
	, mWorldBounds(0.f, 0.f, mWorldView.getSize().x, 768.f)
	, mSpawnPosition(mWorldView.getSize().x / 2.f, mWorldBounds.height - mWorldView.getSize().y / 2.f)
	, mScrollSpeed(0)
	, mScrollSpeedCompensation(0)
	//, mPlayerAircrafts()
	, mPlayerCommanders()
	, mCollisionLeft(false)
	, mCollisionRight(false)
	, mCollisionDown(false)
	, mCollisionUp(false)
	//, mEnemySpawnPoints()
	//, mActiveEnemies()
	, mNetworkedWorld(networked)
	, mNetworkNode(nullptr)
	, mFinishSprite(nullptr)
{
	mSceneTexture.create(mTarget.getSize().x, mTarget.getSize().y);

	loadTextures();
	buildScene();

	// Prepare the view
	mWorldView.setCenter(mSpawnPosition);
}

void World::setWorldScrollCompensation(float compensation)
{
	mScrollSpeedCompensation = compensation;
}

void World::update(sf::Time dt)
{
	// Scroll the world, reset player velocity
	mWorldView.move(0.f, mScrollSpeed * dt.asSeconds() * mScrollSpeedCompensation);

	FOREACH(Commander* c, mPlayerCommanders)
		c->setVelocity(0.f, 0.f);

	// Setup commands to destroy entities, and guide missiles
	destroyEntitiesOutsideView();
	//guideMissiles();

	// Forward commands to scene graph, adapt velocity (scrolling, diagonal correction)
	while (!mCommandQueue.isEmpty())
		mSceneGraph.onCommand(mCommandQueue.pop(), dt);

	adaptPlayerVelocity();

	// Collision detection and response (may destroy entities)
	handleCollisions();

	// Remove aircrafts that were destroyed (World::removeWrecks() only destroys the entities, not the pointers in mPlayerAircraft)
	/*auto firstToRemove = std::remove_if(mPlayerAircrafts.begin(), mPlayerAircrafts.end(), std::mem_fn(&Aircraft::isMarkedForRemoval));
	mPlayerAircrafts.erase(firstToRemove, mPlayerAircrafts.end());*/

	// Remove players that were destroyed (World::removeWrecks() only destroys the entities, not the pointers in players)
	auto firstToRemove = std::remove_if(mPlayerCommanders.begin(), mPlayerCommanders.end(), std::mem_fn(&Commander::isMarkedForRemoval));
	mPlayerCommanders.erase(firstToRemove, mPlayerCommanders.end());

	// Remove all destroyed entities, create new ones
	mSceneGraph.removeWrecks();
	//spawnEnemies();

	// Regular update step, adapt position (correct if outside view)
	mSceneGraph.update(dt, mCommandQueue);
	adaptPlayerPosition();

	updateSounds();
}

void World::draw()
{
	if (PostEffect::isSupported())
	{
		mSceneTexture.clear();
		mSceneTexture.setView(mWorldView);
		mSceneTexture.draw(mSceneGraph);
		mSceneTexture.display();
		mBloomEffect.apply(mSceneTexture, mTarget);
	}
	else
	{
		mTarget.setView(mWorldView);
		mTarget.draw(mSceneGraph);
	}
}

CommandQueue& World::getCommandQueue()
{
	return mCommandQueue;
}

Aircraft* World::getAircraft(int identifier) const
{
	

	return nullptr;
}

void World::removeAircraft(int identifier)
{
	Aircraft* aircraft = getAircraft(identifier);
	if (aircraft)
	{
		aircraft->destroy();
	}
}

//Aircraft* World::addAircraft(int identifier)
//{
//	std::unique_ptr<Aircraft> player1(new Aircraft(Aircraft::Eagle, mTextures, mFonts));
//	player1->setPosition(mWorldView.getCenter());
//	player1->setIdentifier(identifier);
//
//	//mPlayerAircrafts.push_back(player1.get());
//	mSceneLayers[UpperAir]->attachChild(std::move(player1));
//	//return mPlayerAircrafts.back();
//}

Commander* World::getCommander(int identifier) const
{
	FOREACH(Commander* c, mPlayerCommanders)
	{
		if (c->getIdentifier() == identifier)
			return c;
	}

	return nullptr;
}

void World::removeCommander(int identifier)
{
	Commander* commader = getCommander(identifier);
	if (commader)
	{
		commader->destroy();
		mPlayerCommanders.erase(std::find(mPlayerCommanders.begin(), mPlayerCommanders.end(), commader));
	}
}

Commander* World::addCommander(int identifier)
{
	std::cout << "Identifer" << identifier << std::endl;
	
	if(identifier > 0 && identifier < 12)
	{
		Commander::Type playerType[10] = { Commander::Type::Player1, Commander::Type::Player2,Commander::Type::Player3, Commander::Type::Player4, Commander::Type::Player5, Commander::Type::Player6, Commander::Type::Player7, Commander::Type::Player8, Commander::Type::Player9, Commander::Type::Player10 };
		sf::Vector2f SpawnLocations[10] = { sf::Vector2f(70.f, 70.f), sf::Vector2f(410.f, 70.f), sf::Vector2f(1250.f, 70.f), sf::Vector2f(70.f, 365.f), sf::Vector2f(70.f, 700.f),
			sf::Vector2f(410.f, 700.f), sf::Vector2f(1250.f, 700.f), sf::Vector2f(1250.f, 365.f), sf::Vector2f(820.f, 70.f), sf::Vector2f(820.f, 700.f) };

		std::unique_ptr<Commander> player(new Commander(playerType[identifier-1], mTextures, mFonts));
		player->setPosition(SpawnLocations[identifier-1]);
		player->setIdentifier(identifier);

		mPlayerCommanders.push_back(player.get());
		mSceneLayers[UpperAir]->attachChild(std::move(player));

	}
	std::cout << mPlayerCommanders.size();
	return mPlayerCommanders.back();

}

void World::createPickup(sf::Vector2f position, Pickup::Type type)
{
	std::unique_ptr<Pickup> pickup(new Pickup(type, mTextures));
	pickup->setPosition(position);
	pickup->setVelocity(0.f, 1.f);
	mSceneLayers[UpperAir]->attachChild(std::move(pickup));
}

bool World::pollGameAction(GameActions::Action& out)
{
	return mNetworkNode->pollGameAction(out);
}

void World::setCurrentBattleFieldPosition(float lineY)
{
	mWorldView.setCenter(mWorldView.getCenter().x, lineY - mWorldView.getSize().y / 2);
	mSpawnPosition.y = mWorldBounds.height;
}

void World::setWorldHeight(float height)
{
	mWorldBounds.height = height;
}

void World::setMap(int map)
{
	mMapChosen = map;
	std::cout << "mpMap: "  << map << " WorldMap: " << mMapChosen << std::endl;

}

bool World::hasAlivePlayer() const
{
	return mPlayerCommanders.size() > 0;
}

bool World::hasPlayerReachedEnd() const
{
	if (Aircraft* aircraft = getAircraft(1))
		return !mWorldBounds.contains(aircraft->getPosition());
	else
		return false;
}

void World::loadTextures()
{
	mTextures.load(Textures::Entities, "Media/Textures/Entities.png");
	mTextures.load(Textures::Players, "Media/Textures/Players.png");
	mTextures.load(Textures::Pickups, "Media/Textures/pickups.png");
	mTextures.load(Textures::Level, "Media/Textures/Level.png");
	mTextures.load(Textures::Jungle, "Media/Textures/Jungle.png");
	mTextures.load(Textures::Explosion, "Media/Textures/Explosion.png");
	mTextures.load(Textures::Particle, "Media/Textures/Particle.png");
	mTextures.load(Textures::FinishLine, "Media/Textures/FinishLine.png");
}

void World::adaptPlayerPosition()
{
	// Keep player's position inside the screen bounds, at least borderDistance units from the border
	sf::FloatRect viewBounds = getViewBounds();
	const float borderDistanceRight = 65.f;
	const float borderDistance = 40.f;

	FOREACH(Commander* commander, mPlayerCommanders)
	{
		sf::Vector2f position = commander->getPosition();
		position.x = std::max(position.x, viewBounds.left + borderDistanceRight);
		position.x = std::min(position.x, viewBounds.left + viewBounds.width - borderDistanceRight);
		position.y = std::max(position.y, viewBounds.top + borderDistanceRight);
		position.y = std::min(position.y, viewBounds.top + viewBounds.height - borderDistance);
		commander->setPosition(position);
	}
}

void World::adaptPlayerVelocity()
{
	FOREACH(Commander* commander, mPlayerCommanders)
	{
		sf::Vector2f velocity = commander->getVelocity();

		// If moving diagonally, reduce velocity (to have always same velocity)
		if (velocity.x != 0.f && velocity.y != 0.f)
			commander->setVelocity(velocity / std::sqrt(2.f));

		// Add scrolling velocity
		commander->accelerate(0.f, mScrollSpeed);
	}
}

bool matchesCategories(SceneNode::Pair& colliders, Category::Type type1, Category::Type type2)
{
	unsigned int category1 = colliders.first->getCategory();
	unsigned int category2 = colliders.second->getCategory();

	// Make sure first pair entry has category type1 and second has type2
	if (type1 & category1 && type2 & category2)
	{
		return true;
	}
	else if (type1 & category2 && type2 & category1)
	{
		std::swap(colliders.first, colliders.second);
		return true;
	}
	else
	{
		return false;
	}
}

void World::handleCollisions()
{
	std::set<SceneNode::Pair> collisionPairs;
	mSceneGraph.checkSceneCollision(mSceneGraph, collisionPairs);

	FOREACH(SceneNode::Pair pair, collisionPairs)
	{

		if (matchesCategories(pair, Category::Player, Category::Wall))
		{
			auto& player = static_cast<Commander&>(*pair.first);
			auto& wall = static_cast<Wall&>(*pair.second);

			//sf::Vector2f playerLocation = player.getPosition();

			//try Collision Detect with sprite dir & 

			if (player.getVelocity().x > 0.f)
				mCollisionRight = true;
			if (player.getVelocity().x < 0.f)
				mCollisionLeft = true;
			if (player.getVelocity().y > 0.f)
				mCollisionDown = true;
			if (player.getVelocity().y < 0.f)
				mCollisionUp = true;



			if (mCollisionLeft)
			{
				//std::cout << "Collision Left! " << std::endl;
				//player.setPosition(playerLocation.x + 5.f, playerLocation.y);
				//player.setVelocityX(0);

				sf::FloatRect intersection;

				//player.getVelocity().x > 0.f ? player.setVelocityX(0.f), player.setPosition(player.getPosition().x + 2.f, player.getPosition().y) : player.setVelocityX(-5.f);

				/*player.setVelocity(0, 0);
				if (player.getBoundingRect().intersects(wall.getBoundingRect(), intersection))
					player.move(intersection.width, 0.0f);*/

				player.getVelocity().x < 0.f ? player.setVelocityX(0.f), player.setPosition(player.getPosition().x + 2.f, player.getPosition().y) : player.setVelocityX(5.f);

				/*player.setVelocityX(0);
				if (player.getVelocity().x < 0.f)
				player.setVelocityX(player.getMaxSpeed());
				else
				player.setVelocityX(0);*/
				//mCollisionLeft = false;
			}
			if (mCollisionRight)
			{
				//std::cout << "Collision Right! " << std::endl;
				//player.setPosition(playerLocation.x - 5.f, playerLocation.y);
				//player.setVelocityX(0);
				
				sf::FloatRect intersection;

				//player.getVelocity().x > 0.f ? player.setVelocityX(0.f), player.setPosition(player.getPosition().x - 2.f, player.getPosition().y) : player.setVelocityX(-5.f);


				//player.setVelocity(0,0);
				/*if (player.getBoundingRect().intersects(wall.getBoundingRect(), intersection))
					player.getVelocity().x > 0.f ? player.setVelocityX(0.f), player.setPosition(player.getPosition().x - intersection.width, player.getPosition().y) : player.setVelocityX(-5.f);*/


				player.getVelocity().x > 0.f ? player.setVelocityX(0.f), player.setPosition(player.getPosition().x - 2.f, player.getPosition().y) : player.setVelocityX(-5.f);

				//mCollisionRight = false;
			}
			if (mCollisionDown)
			{
				//sf::FloatRect intersection;

				//player.getVelocity().x > 0.f ? player.setVelocityX(0.f), player.setPosition(player.getPosition().x, player.getPosition().y - 2.f) : player.setVelocityX(-5.f);

				/*player.setVelocity(0,0);
				if (player.getBoundingRect().intersects(wall.getBoundingRect(), intersection))
					player.move(0.0f, -intersection.height);*/
				//std::cout << "Collision Down! " << std::endl;
				//player.setPosition(playerLocation.x, playerLocation.y - 5.f);

				player.getVelocity().y > 0.f ? player.setVelocityY(0.f), player.setPosition(player.getPosition().x, player.getPosition().y - 2.f) : player.setVelocityY(-5.f);

				/*player.setVelocityY(0);*/
				//mCollisionDown = false;
			}
			if (mCollisionUp)
			{
				sf::FloatRect intersection;

				//player.getVelocity().x > 0.f ? player.setVelocityX(0.f), player.setPosition(player.getPosition().x, player.getPosition().y + 2.f) : player.setVelocityX(-5.f);

				/*if (player.getBoundingRect().intersects(wall.getBoundingRect(), intersection))
					player.getVelocity().x > 0.f ? player.setVelocityX(0.f), player.setPosition(player.getPosition().x - intersection.width, player.getPosition().y) : player.setVelocityX(-5.f);
*/
				////std::cout << "Collision Up! " << std::endl;
				////player.setPosition(playerLocation.x, playerLocation.y + 5.f);

				player.getVelocity().y < 0.f ? player.setVelocityY(0.f), player.setPosition(player.getPosition().x, player.getPosition().y + 2.f) : player.setVelocityY(5.f);

				///*player.setVelocityY(0);*/
				//mCollisionUp = false;
			}
		}

		else if (matchesCategories(pair, Category::Player, Category::Pickup))
		{
			auto& player = static_cast<Commander&>(*pair.first);
			auto& pickup = static_cast<Pickup&>(*pair.second);

			// Apply pickup effect to player, destroy projectile
			pickup.apply(player);
			pickup.destroy();
			player.playLocalSound(mCommandQueue, SoundEffect::CollectPickup);
		}

		else if (matchesCategories(pair, Category::AlliedProjectile, Category::Wall) ||
			matchesCategories(pair, Category::EnemyProjectile, Category::Wall))
		{
			auto& bullet = static_cast<Projectile&>(*pair.first);
			auto& wall = static_cast<Wall&>(*pair.second);

			bullet.destroy();
		}

		else if (matchesCategories(pair, Category::Player, Category::AlliedProjectile)
			|| matchesCategories(pair, Category::Player, Category::EnemyProjectile))
		{
			auto& commander = static_cast<Commander&>(*pair.first);
			auto& projectile = static_cast<Projectile&>(*pair.second);

			// Apply projectile damage to aircraft, destroy projectile
			commander.damage(projectile.getDamage());
			projectile.destroy();
		}
	}
}

void World::updateSounds()
{
	sf::Vector2f listenerPosition;

	// 0 players (multiplayer mode, until server is connected) -> view center
	if (mPlayerCommanders.empty())
	{
		listenerPosition = mWorldView.getCenter();
	}

	// 1 or more players -> mean position between all aircrafts
	else
	{
		FOREACH(Commander* commander, mPlayerCommanders)
			listenerPosition += commander->getWorldPosition();

		listenerPosition /= static_cast<float>(mPlayerCommanders.size());
	}

	// Set listener's position
	mSounds.setListenerPosition(listenerPosition);

	// Remove unused sounds
	mSounds.removeStoppedSounds();
}

void World::buildScene()
{
	// Initialize the different layers
	for (std::size_t i = 0; i < LayerCount; ++i)
	{
		Category::Type category = (i == LowerAir) ? Category::SceneAirLayer : Category::None;

		SceneNode::Ptr layer(new SceneNode(category));
		mSceneLayers[i] = layer.get();

		mSceneGraph.attachChild(std::move(layer));
	}

	sf::Texture& levelTexture = mTextures.get(Textures::Level);

	/*createWalls(sf::Vector2f(20, 100), sf::Vector2f(200, 150));
	createWalls(sf::Vector2f(200, 20), sf::Vector2f(200, 150));

	createWalls(sf::Vector2f(20, -100), sf::Vector2f(200, 618));
	createWalls(sf::Vector2f(200, 20), sf::Vector2f(200, 618));

	createWalls(sf::Vector2f(20, 100), sf::Vector2f(1146, 150));
	createWalls(sf::Vector2f(200, 20), sf::Vector2f(966, 150));

	createWalls(sf::Vector2f(20, 100), sf::Vector2f(1146, 518));
	createWalls(sf::Vector2f(200, 20), sf::Vector2f(966, 618));

	createWalls(sf::Vector2f(20, 100), sf::Vector2f((mWorldBounds.width / 2.f) + 100.f, (mWorldBounds.height / 2.f) + 80.f));
	createWalls(sf::Vector2f(150, 20), sf::Vector2f((mWorldBounds.width / 2.f) + 100.f, (mWorldBounds.height / 2.f) + 80.f));

	createWalls(sf::Vector2f(20, 100), sf::Vector2f((mWorldBounds.width / 2.f) - 120.f, (mWorldBounds.height / 2.f) + 80.f));
	createWalls(sf::Vector2f(-150, 20), sf::Vector2f((mWorldBounds.width / 2.f) - 100.f, (mWorldBounds.height / 2.f) + 80.f));

	createWalls(sf::Vector2f(20, -100), sf::Vector2f((mWorldBounds.width / 2.f) - 120.f, (mWorldBounds.height / 2.f) - 80.f));
	createWalls(sf::Vector2f(-150, 20), sf::Vector2f((mWorldBounds.width / 2.f) - 100.f, (mWorldBounds.height / 2.f) - 80.f));

	createWalls(sf::Vector2f(20, -100), sf::Vector2f((mWorldBounds.width / 2.f) + 100.f, (mWorldBounds.height / 2.f) - 80.f));
	createWalls(sf::Vector2f(150, 20), sf::Vector2f((mWorldBounds.width / 2.f) + 100.f, (mWorldBounds.height / 2.f) - 80.f));*/

	std::unique_ptr<SpriteNode> levelSprite(new SpriteNode(levelTexture));
	levelSprite->setPosition(mWorldBounds.left, mWorldBounds.top);
	mSceneLayers[Background]->attachChild(std::move(levelSprite));

	//// Prepare the tiled background
	//sf::Texture& jungleTexture = mTextures.get(Textures::Jungle);
	//jungleTexture.setRepeated(true);

	//float viewHeight = mWorldView.getSize().y;
	//sf::IntRect textureRect(mWorldBounds);
	//textureRect.height += static_cast<int>(viewHeight);

	//// Add the background sprite to the scene
	//std::unique_ptr<SpriteNode> jungleSprite(new SpriteNode(jungleTexture, textureRect));
	//jungleSprite->setPosition(mWorldBounds.left, mWorldBounds.top - viewHeight);
	//mSceneLayers[Background]->attachChild(std::move(jungleSprite));

	// Add the finish line to the scene
	/*sf::Texture& finishTexture = mTextures.get(Textures::FinishLine);
	std::unique_ptr<SpriteNode> finishSprite(new SpriteNode(finishTexture));
	finishSprite->setPosition(0.f, -76.f);
	mFinishSprite = finishSprite.get();
	mSceneLayers[Background]->attachChild(std::move(finishSprite));*/

	// Add particle node to the scene
	std::unique_ptr<ParticleNode> smokeNode(new ParticleNode(Particle::Smoke, mTextures));
	mSceneLayers[LowerAir]->attachChild(std::move(smokeNode));

	// Add propellant particle node to the scene
	std::unique_ptr<ParticleNode> propellantNode(new ParticleNode(Particle::Propellant, mTextures));
	mSceneLayers[LowerAir]->attachChild(std::move(propellantNode));

	// Add sound effect node
	std::unique_ptr<SoundNode> soundNode(new SoundNode(mSounds));
	mSceneGraph.attachChild(std::move(soundNode));

	// Add network node, if necessary
	if (mNetworkedWorld)
	{
		std::unique_ptr<NetworkNode> networkNode(new NetworkNode());
		mNetworkNode = networkNode.get();
		mSceneGraph.attachChild(std::move(networkNode));
	}
}

void World::createLevel()
{
	switch (mMapChosen)
	{
	case 1:
		createWalls(sf::Vector2f(20, 100), sf::Vector2f(200, 150));
		createWalls(sf::Vector2f(200, 20), sf::Vector2f(200, 150));

		createWalls(sf::Vector2f(20, -100), sf::Vector2f(200, 618));
		createWalls(sf::Vector2f(200, 20), sf::Vector2f(200, 618));

		createWalls(sf::Vector2f(20, 100), sf::Vector2f(1146, 150));
		createWalls(sf::Vector2f(200, 20), sf::Vector2f(966, 150));

		createWalls(sf::Vector2f(20, 100), sf::Vector2f(1146, 518));
		createWalls(sf::Vector2f(200, 20), sf::Vector2f(966, 618));

		createWalls(sf::Vector2f(20, 100), sf::Vector2f((mWorldBounds.width / 2.f) + 100.f, (mWorldBounds.height / 2.f) + 80.f));
		createWalls(sf::Vector2f(150, 20), sf::Vector2f((mWorldBounds.width / 2.f) + 100.f, (mWorldBounds.height / 2.f) + 80.f));

		createWalls(sf::Vector2f(20, 100), sf::Vector2f((mWorldBounds.width / 2.f) - 120.f, (mWorldBounds.height / 2.f) + 80.f));
		createWalls(sf::Vector2f(-150, 20), sf::Vector2f((mWorldBounds.width / 2.f) - 100.f, (mWorldBounds.height / 2.f) + 80.f));

		createWalls(sf::Vector2f(20, -100), sf::Vector2f((mWorldBounds.width / 2.f) - 120.f, (mWorldBounds.height / 2.f) - 80.f));
		createWalls(sf::Vector2f(-150, 20), sf::Vector2f((mWorldBounds.width / 2.f) - 100.f, (mWorldBounds.height / 2.f) - 80.f));

		createWalls(sf::Vector2f(20, -100), sf::Vector2f((mWorldBounds.width / 2.f) + 100.f, (mWorldBounds.height / 2.f) - 80.f));
		createWalls(sf::Vector2f(150, 20), sf::Vector2f((mWorldBounds.width / 2.f) + 100.f, (mWorldBounds.height / 2.f) - 80.f));
		break;

	case 2:
		createWalls(sf::Vector2f(900, 20), sf::Vector2f(200, 150));
		createWalls(sf::Vector2f(900, 20), sf::Vector2f(200, 618));

		createWalls(sf::Vector2f(20, 268), sf::Vector2f(200, 264));
		createWalls(sf::Vector2f(20, 268), sf::Vector2f(1066, 264));

		createWalls(sf::Vector2f(60, 168), sf::Vector2f((mWorldBounds.width / 2.f) - 30.f, 314));
		break;

	case 3:
		createWalls(sf::Vector2f(20, 150), sf::Vector2f(200, 20));
		createWalls(sf::Vector2f(20, 300), sf::Vector2f(200, 264));
		createWalls(sf::Vector2f(20, -100), sf::Vector2f(200, 761));

		createWalls(sf::Vector2f(20, 150), sf::Vector2f(1066, 20));
		createWalls(sf::Vector2f(20, 300), sf::Vector2f(1066, 264));
		createWalls(sf::Vector2f(20, -100), sf::Vector2f(1066, 761));

		createWalls(sf::Vector2f(20, 100), sf::Vector2f((mWorldBounds.width / 2.f) - 200.f, (mWorldBounds.height / 2.f) - 150.f));
		createWalls(sf::Vector2f(150, 20), sf::Vector2f((mWorldBounds.width / 2.f) - 200.f, (mWorldBounds.height / 2.f) - 150.f));

		createWalls(sf::Vector2f(20, 100), sf::Vector2f((mWorldBounds.width / 2.f) + 200.f, (mWorldBounds.height / 2.f) - 150.f));
		createWalls(sf::Vector2f(-150, 20), sf::Vector2f((mWorldBounds.width / 2.f) + 220.f, (mWorldBounds.height / 2.f) - 150.f));

		createWalls(sf::Vector2f(20, -100), sf::Vector2f((mWorldBounds.width / 2.f) - 200.f, (mWorldBounds.height / 2.f) + 150.f));
		createWalls(sf::Vector2f(150, 20), sf::Vector2f((mWorldBounds.width / 2.f) - 200.f, (mWorldBounds.height / 2.f) + 150.f));

		createWalls(sf::Vector2f(20, -100), sf::Vector2f((mWorldBounds.width / 2.f) + 200.f, (mWorldBounds.height / 2.f) + 150.f));
		createWalls(sf::Vector2f(-150, 20), sf::Vector2f((mWorldBounds.width / 2.f) + 220.f, (mWorldBounds.height / 2.f) + 150.f));

		break;

	default:
		/*createWalls(sf::Vector2f(900, 20), sf::Vector2f(200, 150));
		createWalls(sf::Vector2f(900, 20), sf::Vector2f(200, 618));

		createWalls(sf::Vector2f(20, 268), sf::Vector2f(200, 264));
		createWalls(sf::Vector2f(20, 268), sf::Vector2f(1066, 264));

		createWalls(sf::Vector2f(60, 168), sf::Vector2f((mWorldBounds.width / 2.f) - 30.f, 314));
		std::cout << "World map1: " << mMapChosen << std::endl;*/

		break;
	}

}

void World::createWalls(sf::Vector2f size, sf::Vector2f position)
{
	std::unique_ptr<Wall> wall(new Wall(size, position));
	mSceneLayers[Layer::UpperAir]->attachChild(std::move(wall));
}

void World::destroyEntitiesOutsideView()
{
	Command command;
	command.category = Category::Projectile | Category::EnemyAircraft;
	command.action = derivedAction<Entity>([this](Entity& e, sf::Time)
	{
		if (!getBattlefieldBounds().intersects(e.getBoundingRect()))
			e.remove();
	});

	mCommandQueue.push(command);
}

sf::FloatRect World::getViewBounds() const
{
	return sf::FloatRect(mWorldView.getCenter() - mWorldView.getSize() / 2.f, mWorldView.getSize());
}

sf::FloatRect World::getBattlefieldBounds() const
{
	// Return view bounds + some area at top, where enemies spawn
	sf::FloatRect bounds = getViewBounds();
	bounds.top -= 100.f;
	bounds.height += 100.f;

	return bounds;
}