#include "../Include/Book/MultiplayerGameState.hpp"
#include "../Include/Book/MusicPlayer.hpp"
#include "../Include/Book/Foreach.hpp"
#include "../Include/Book/Utility.hpp"

#include <SFML/Graphics/RectangleShape.hpp>
#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/Network/IpAddress.hpp>

#include <fstream>
#include <iostream>

sf::IpAddress getAddressFromFile()
{
	{ // Try to open existing file (RAII block)
		std::ifstream inputFile("ip.txt");
		std::string ipAddress;
		if (inputFile >> ipAddress)
			return ipAddress;
	}

	// If open/read failed, create new file
	std::ofstream outputFile("ip.txt");
	std::string localAddress = "127.0.0.1";
	outputFile << localAddress;
	return localAddress;
}

MultiplayerGameState::MultiplayerGameState(StateStack& stack, Context context, bool isHost)
	: State(stack, context)
	, mWorld(*context.window, *context.fonts, *context.sounds, true)
	, mWindow(*context.window)
	, mTextureHolder(*context.textures)
	, mConnected(false)
	, mGameServer(nullptr)
	, mActiveState(true)
	, mHasFocus(true)
	, mHost(isHost)
	, mGameStarted(false)
	, mClientTimeout(sf::seconds(2.f))
	, mTimeSinceLastPacket(sf::seconds(0.f))
	, mMap1Counter(static_cast<sf::Int32>(0))
	, mMap2Counter(static_cast<sf::Int32>(0))
	, mMap3Counter(static_cast<sf::Int32>(0))
	, mMapChosen(0)
	, mGameOverText()
{
	mBroadcastText.setFont(context.fonts->get(Fonts::Main));
	mBroadcastText.setPosition(1024.f / 2, 100.f);


	// Additional Stuff
	mName = getName();

	sf::Vector2f windowSize(context.window->getSize());

	mGameOverText.setFont(context.fonts->get(Fonts::Main));
	mGameOverText.setString("You Are Not Alive! |-_-|");
	mGameOverText.setCharacterSize(70);
	centerOrigin(mGameOverText);
	mGameOverText.setPosition(0.5f * windowSize.x, 0.4f * windowSize.y);

	if (isHost)
	{
		mHostText.setString("You are the Host! type'start' to play");
		mHostText.setFont(context.fonts->get(Fonts::Main));
		mHostText.setStyle(sf::Text::Italic);
		mHostText.setCharacterSize(20);
		centerOrigin(mHostText);
		mHostText.setPosition(mWindow.getSize().x / 2.f, 100);
	}

	mVoteText.setString("type 'votemap1' or 'votemap2' or 'votemap3' to vote for a map!");
	mVoteText.setFont(context.fonts->get(Fonts::Main));
	mVoteText.setStyle(sf::Text::Italic);
	mVoteText.setCharacterSize(20);
	centerOrigin(mVoteText);
	mVoteText.setPosition(mWindow.getSize().x / 2.f, 125);

	mTitle.setString("Lobby");
	//mTitle.setString("Lobby\nType 'votemap1, 2 or 3 to choose map.'");
	mTitle.setFont(context.fonts->get(Fonts::Main));
	mTitle.setCharacterSize(50);
	centerOrigin(mTitle);
	mTitle.setPosition(mWindow.getSize().x / 2.f, 50);

	mUserCountText.setString("Players Connected (" + std::to_string(mPlayers.size()) + "/10)");
	mUserCountText.setFont(context.fonts->get(Fonts::Main));
	mUserCountText.setCharacterSize(20);
	mUserCountText.setPosition(1000, 150);

	

	mChatInput = "Chat: ";

	mLobbyText.setString(mChatInput);
	mLobbyText.setFont(context.fonts->get(Fonts::Main));
	mLobbyText.setCharacterSize(20);
	mLobbyText.setPosition(25, 700);
	initialiseChatLog(context);

	// We reuse this text for "Attempt to connect" and "Failed to connect" messages
	mFailedConnectionText.setFont(context.fonts->get(Fonts::Main));
	mFailedConnectionText.setString("Attempting to connect...");
	mFailedConnectionText.setCharacterSize(35);
	//mFailedConnectionText.setColor(sf::Color::White);
	centerOrigin(mFailedConnectionText);
	mFailedConnectionText.setPosition(mWindow.getSize().x / 2.f, mWindow.getSize().y / 2.f);

	// Render a "establishing connection" frame for user feedback
	mWindow.clear(sf::Color::Black);
	mWindow.draw(mFailedConnectionText);
	mWindow.display();
	mFailedConnectionText.setString("Could not connect to the remote server!");
	centerOrigin(mFailedConnectionText);

	sf::IpAddress ip;
	if (isHost)
	{
		mGameServer.reset(new GameServer(sf::Vector2f(mWindow.getSize())));
		ip = "127.0.0.1";
	}
	else
	{
		ip = getAddressFromFile();
	}

	if (mSocket.connect(ip, ServerPort, sf::seconds(5.f)) == sf::TcpSocket::Done)
		mConnected = true;
	else
		mFailedConnectionClock.restart();

	mSocket.setBlocking(false);

	// Play game theme
	context.music->play(Music::MissionTheme);
}

void MultiplayerGameState::draw()
{
	if (mConnected)
	{
		if (mGameStarted)
		{
			mWorld.draw();
		}
		// Otherwise draw the lobby area
		else
		{
			mWindow.draw(mTitle);
			mWindow.draw(mHostText);
			mWindow.draw(mVoteText);
			mWindow.draw(mUserCountText);
			mWindow.draw(mLobbyText);
			for (int i = 0; i < mChatMessages.size(); i++)
			{
				mWindow.draw(mChatMessages[i]);
			}
			for (int i = 0; i < mUsersLoggedOn.size(); i++)
			{
				mWindow.draw(mUsersLoggedOn[i]);
			}

		}
		// Broadcast messages in default view
		mWindow.setView(mWindow.getDefaultView());

		if (!mBroadcasts.empty())
			mWindow.draw(mBroadcastText);

		if (mLocalPlayerIdentifiers.size() < 2 && mPlayerInvitationTime < sf::seconds(0.5f))
			mWindow.draw(mPlayerInvitationText);
	}
	else
	{
		mWindow.draw(mFailedConnectionText);
	}
}

void MultiplayerGameState::onActivate()
{
	mActiveState = true;
}

void MultiplayerGameState::onDestroy()
{
	if (!mHost && mConnected)
	{
		// Inform server this client is dying
		/*sf::Packet packet;
		packet << static_cast<sf::Int32>(Client::Quit);
		mSocket.send(packet);*/
		drawDeathScreen();
	}
}

void MultiplayerGameState::drawDeathScreen()
{
	sf::RenderWindow& window = *getContext().window;
	window.setView(window.getDefaultView());

	// Create dark, semitransparent background
	sf::RectangleShape backgroundShape;
	backgroundShape.setFillColor(sf::Color(0, 0, 0, 150));
	backgroundShape.setSize(window.getView().getSize());

	window.draw(backgroundShape);
	window.draw(mGameOverText);
}

void MultiplayerGameState::drawGameOverScreen()
{
	mGameOverText.setString("Game Over!");

	sf::RenderWindow& window = *getContext().window;
	window.setView(window.getDefaultView());

	// Create dark, semitransparent background
	sf::RectangleShape backgroundShape;
	backgroundShape.setFillColor(sf::Color(0, 0, 0, 150));
	backgroundShape.setSize(window.getView().getSize());

	window.draw(backgroundShape);
	window.draw(mGameOverText);
}

bool MultiplayerGameState::update(sf::Time dt)
{
	// Connected to server: Handle all the network logic
	if (mConnected)
	{
		
		if (mGameStarted)
		{
			mWorld.update(dt);
		}
		else 
		{
			if (mMap3Counter > mMap2Counter && mMap3Counter > mMap1Counter)
			{
				mMapChosen = 3;
			}
			else if (mMap2Counter > mMap1Counter)
			{
				mMapChosen = 2;
			}
			else
			{
				mMapChosen = 1;
			}
		}

		// Remove players whose aircrafts were destroyed
		bool foundLocalPlane = false;
		for (auto itr = mPlayers.begin(); itr != mPlayers.end(); )
		{
			// Check if there are no more local planes for remote clients
			if (std::find(mLocalPlayerIdentifiers.begin(), mLocalPlayerIdentifiers.end(), itr->first) != mLocalPlayerIdentifiers.end())
			{
				foundLocalPlane = true;
			}

			//if (!mWorld.getAircraft(itr->first))
			if (!mWorld.getCommander(itr->first))
			{
				itr = mPlayers.erase(itr);

				// No more players left: Mission failed
				if (mPlayers.empty())
					requestStackPush(States::GameOver);
			}
			else
			{
				++itr;
			}
		}

		if (!foundLocalPlane && mGameStarted)
		{
			drawDeathScreen();
			//requestStackPush(States::GameOver);
		}

		// Only handle the realtime input if the window has focus and the game is unpaused
		if (mActiveState && mHasFocus)
		{
			CommandQueue& commands = mWorld.getCommandQueue();
			FOREACH(auto& pair, mPlayers)
				pair.second->handleRealtimeInput(commands);
		}

		// Always handle the network input
		CommandQueue& commands = mWorld.getCommandQueue();
		FOREACH(auto& pair, mPlayers)
			pair.second->handleRealtimeNetworkInput(commands);

		// Handle messages from server that may have arrived
		sf::Packet packet;
		if (mSocket.receive(packet) == sf::Socket::Done)
		{
			mTimeSinceLastPacket = sf::seconds(0.f);
			sf::Int32 packetType;
			packet >> packetType;
			handlePacket(packetType, packet);
		}
		else
		{
			// Check for timeout with the server
			if (mTimeSinceLastPacket > mClientTimeout)
			{
				mConnected = false;

				mFailedConnectionText.setString("Lost connection to server");
				centerOrigin(mFailedConnectionText);

				mFailedConnectionClock.restart();
			}
		}

		updateBroadcastMessage(dt);

		// Events occurring in the game
		GameActions::Action gameAction;
		while (mWorld.pollGameAction(gameAction))
		{
			sf::Packet packet;
			packet << static_cast<sf::Int32>(Client::GameEvent);
			packet << static_cast<sf::Int32>(gameAction.type);
			packet << gameAction.position.x;
			packet << gameAction.position.y;

			mSocket.send(packet);
		}

		// Regular position updates
		if (mTickClock.getElapsedTime() > sf::seconds(1.f / 20.f))
		{
			sf::Packet positionUpdatePacket;
			positionUpdatePacket << static_cast<sf::Int32>(Client::PositionUpdate);
			positionUpdatePacket << static_cast<sf::Int32>(mLocalPlayerIdentifiers.size());

			FOREACH(sf::Int32 identifier, mLocalPlayerIdentifiers)
			{
				/*if (Aircraft* aircraft = mWorld.getAircraft(identifier))
					positionUpdatePacket << identifier << aircraft->getPosition().x << aircraft->getPosition().y << static_cast<sf::Int32>(aircraft->getHitpoints()) << static_cast<sf::Int32>(aircraft->getMissileAmmo());*/
				if (Commander* commander = mWorld.getCommander(identifier))
					positionUpdatePacket << identifier << commander->getPosition().x << commander->getPosition().y << static_cast<sf::Int32>(commander->getHitpoints());
			}

			mSocket.send(positionUpdatePacket);
			mTickClock.restart();
		}

		mTimeSinceLastPacket += dt;
	}

	// Failed to connect and waited for more than 5 seconds: Back to menu
	else if (mFailedConnectionClock.getElapsedTime() >= sf::seconds(5.f))
	{
		requestStateClear();
		requestStackPush(States::Menu);
	}

	return true;
}

void MultiplayerGameState::disableAllRealtimeActions()
{
	mActiveState = false;

	FOREACH(sf::Int32 identifier, mLocalPlayerIdentifiers)
		mPlayers[identifier]->disableAllRealtimeActions();
}

bool MultiplayerGameState::handleEvent(const sf::Event& event)
{
	// Game input handling
	CommandQueue& commands = mWorld.getCommandQueue();

	// Forward event to all players
	FOREACH(auto& pair, mPlayers)
		pair.second->handleEvent(event, commands);
	if (event.type == sf::Event::KeyPressed)
	{
		if (event.key.code == sf::Keyboard::Return)
		{
			// If in the lobby and "Enter" pressed, send the chat input to the server
			if (!mGameStarted)
			{
				sf::Packet packet;
				mChatInput.erase(0, 6);

				if (mChatInput == "start" && mHost)
				{
					sf::Packet packet;
					packet << static_cast<sf::Int32>(Client::GameReady);

					mSocket.send(packet);
				}
				else if (mChatInput == "votemap1")
				{

					mMap1Counter ++;

					sf::Packet mapPacket;
					mapPacket << static_cast<sf::Int32>(Client::MapVoting) << static_cast<sf::Int32>(mMap1Counter) << static_cast<sf::Int32>(mMap2Counter) << static_cast<sf::Int32>(mMap3Counter);


					mSocket.send(mapPacket);

					std::cout << "VoteMap1"  << mMap1Counter << std::endl;
					std::cout << mMap1Counter << mMap2Counter << mMap3Counter << std::endl;


				}
				else if (mChatInput == "votemap2")
				{
					mMap2Counter ++;

					sf::Packet mapPacket;
					mapPacket << static_cast<sf::Int32>(Client::MapVoting) << static_cast<sf::Int32>(mMap1Counter) << static_cast<sf::Int32>(mMap2Counter) << static_cast<sf::Int32>(mMap3Counter);
					//std::cout << mMap1Counter << mMap2Counter << mMap3Counter << std::endl;

					mSocket.send(mapPacket);

					std::cout << "VoteMap2" << mMap2Counter << std::endl;


				}
				else if (mChatInput == "votemap3")
				{
					mMap3Counter ++;
					//std::cout << "VoteMap3" << std::endl;

					sf::Packet mapPacket;
					mapPacket << static_cast<sf::Int32>(Client::MapVoting) << static_cast<sf::Int32>(mMap1Counter) << static_cast<sf::Int32>(mMap2Counter) << static_cast<sf::Int32>(mMap3Counter);
					//std::cout << mMap1Counter << mMap2Counter << mMap3Counter << std::endl;
					mSocket.send(mapPacket);
					std::cout << "VoteMap3" << mMap3Counter << std::endl;
					 
				}


				else
				{
					mChatInput.insert(0, "[" + mName + "] ");
					packet << static_cast<sf::Int32>(Client::SendChatMessage) << mChatInput;

					mSocket.send(packet);
					std::cout << mChatInput << std::endl;
				}

				mChatInput = "Chat:";
			}

		}
		else if (event.key.code == sf::Keyboard::BackSpace)
		{
			if (!mGameStarted)
			{
				if (mChatInput.length() > 6)
				{
					mChatInput = mChatInput.substr(0, mChatInput.size() - 1);
				}
			}

		}
		// Escape pressed, trigger the pause screen
		else if (event.key.code == sf::Keyboard::Escape)
		{
			disableAllRealtimeActions();
			requestStackPush(States::NetworkPause);
		}
	}

	else if (event.type == sf::Event::TextEntered)
	{
		if (!mGameStarted)
		{
			if (32 < event.text.unicode < 128)
			{
				if (event.text.unicode != '\b')
				{
					if (mChatInput.length() <= 140)
					{
						if (mChatInput.length() == 70)
						{
							mChatInput += '\n';
						}
						mChatInput += static_cast<char>(event.text.unicode);
					}
				}
				mLobbyText.setString(mChatInput);

			}
		}
	}
	else if (event.type == sf::Event::GainedFocus)
	{
		mHasFocus = true;
	}
	else if (event.type == sf::Event::LostFocus)
	{
		mHasFocus = false;
	}

	return true;
}

void MultiplayerGameState::updateBroadcastMessage(sf::Time elapsedTime)
{
	if (mBroadcasts.empty())
		return;

	// Update broadcast timer
	mBroadcastElapsedTime += elapsedTime;
	if (mBroadcastElapsedTime > sf::seconds(2.5f))
	{
		// If message has expired, remove it
		mBroadcasts.erase(mBroadcasts.begin());

		// Continue to display next broadcast message
		if (!mBroadcasts.empty())
		{
			mBroadcastText.setString(mBroadcasts.front());
			centerOrigin(mBroadcastText);
			mBroadcastElapsedTime = sf::Time::Zero;
		}
	}
}

void MultiplayerGameState::handlePacket(sf::Int32 packetType, sf::Packet& packet)
{
	switch (packetType)
	{
		// Send message to all clients
	case Server::BroadcastMessage:
	{
		std::string message;
		packet >> message;
		mBroadcasts.push_back(message);

		// Just added first message, display immediately
		if (mBroadcasts.size() == 1)
		{
			mBroadcastText.setString(mBroadcasts.front());
			centerOrigin(mBroadcastText);
			mBroadcastElapsedTime = sf::Time::Zero;
		}
	} break;

	case Server::JoinLobby:
	{
		std::string name;
		packet >> name;

		mUserCountText.setString("Players Connected (" + std::to_string(mPlayers.size()) + "/10)");
		mUsersLoggedOn[mPlayers.size() - 1].setString(name);
	}break;

	case Server::BroadCastChatMessage:
	{
		std::string message;
		packet >> message;

		std::string temp;
		std::string temp1;
		for (int i = 0; i < mChatMessages.size(); i++)
		{
			if (!temp.empty())
			{
				temp1 = mChatMessages[i].getString();
				mChatMessages[i].setString(temp);
				temp = temp1;
			}
			else
			{
				temp = mChatMessages[i].getString();
			}
		}
		mChatMessages[0].setString(message);
	}break;

	case Server::ChoseMap:
	{
		sf::Int32 map1C, map2C, map3C;
		packet >> map1C >> map2C >> map3C;

		std::cout << "maps: " << static_cast<int>(map1C) << static_cast<int>(map2C) << static_cast<int>(map3C)  << std::endl;
		std::cout << "mapsLocal: " << mMap1Counter << mMap2Counter << mMap3Counter << std::endl;
		mMap1Counter = static_cast<int>(map1C);
		mMap2Counter = static_cast<int>(map2C);
		mMap3Counter = static_cast<int>(map3C);
		
		std::cout << "mapsLocalUpdate: " << mMap1Counter << mMap2Counter << mMap3Counter << std::endl;

	}break;

	// Sent by the server to order to spawn player 1 airplane on connect
	case Server::SpawnSelf:
	{

		sf::Int32 commanderIdentifier;
		sf::Vector2f commanderPosition;
		packet >> commanderIdentifier >> commanderPosition.x >> commanderPosition.y;

		Commander* commander = mWorld.addCommander(commanderIdentifier);
		commander->setPosition(commanderPosition);

		mPlayers[commanderIdentifier].reset(new Player(&mSocket, commanderIdentifier, getContext().keys1));
		mLocalPlayerIdentifiers.push_back(commanderIdentifier);

		// Additional Stuff
		sf::Packet namePacket;

		namePacket << static_cast<sf::Int32>(Client::Join) << mName;
		mSocket.send(namePacket);
	} break;

	case Server::StartGame:
	{
		mWorld.setMap(mMapChosen);
		mWorld.createLevel();
		mGameStarted = true;
	}break;

	//
	case Server::PlayerConnect:
	{

		sf::Int32 commanderIdentifier;
		sf::Vector2f commanderPosition;
		packet >> commanderIdentifier >> commanderPosition.x >> commanderPosition.y;

		Commander* commander = mWorld.addCommander(commanderIdentifier);
		commander->setPosition(commander->getPosition());

		//mPlayers[aircraftIdentifier].reset(new Player(&mSocket, aircraftIdentifier, nullptr));
		mPlayers[commanderIdentifier].reset(new Player(&mSocket, commanderIdentifier, nullptr));
	} break;

	//
	case Server::PlayerDisconnect:
	{
		//sf::Int32 aircraftIdentifier;
		sf::Int32 commanderIdentifier;
		//packet >> aircraftIdentifier;
		packet >> commanderIdentifier;

		mWorld.removeCommander(commanderIdentifier);
		//mWorld.removeAircraft(aircraftIdentifier);
		mPlayers.erase(commanderIdentifier);

		mUserCountText.setString("Players Connected (" + std::to_string(mPlayers.size()) + "/10)");
	} break;

	//
	case Server::InitialState:
	{
		sf::Int32 commanderCount;
		float worldHeight, currentScroll;
		packet >> worldHeight >> currentScroll;

		mWorld.setWorldHeight(worldHeight);
		mWorld.setCurrentBattleFieldPosition(currentScroll);

		packet >> commanderCount;
		for (sf::Int32 i = 0; i < commanderCount; ++i)
		{
			sf::Int32 commanderIdentifier;
			sf::Int32 hitpoints;
			sf::Vector2f commanderPosition;
			packet >> commanderIdentifier >> commanderPosition.x >> commanderPosition.y >> hitpoints;

			Commander* commander = mWorld.addCommander(commanderIdentifier);
			commander->setPosition(commanderPosition);
			commander->setHitpoints(hitpoints);

			mPlayers[commanderIdentifier].reset(new Player(&mSocket, commanderIdentifier, nullptr));
		}
	} break;

	// Player event (like missile fired) occurs
	case Server::PlayerEvent:
	{
		sf::Int32 commanderIdentifier;
		sf::Int32 action;
		packet >> commanderIdentifier >> action;

		auto itr = mPlayers.find(commanderIdentifier);
		if (itr != mPlayers.end())
			itr->second->handleNetworkEvent(static_cast<Player::Action>(action), mWorld.getCommandQueue());
	} break;

	// Player's movement or fire keyboard state changes
	case Server::PlayerRealtimeChange:
	{
		sf::Int32 commanderIdentifier;
		//sf::Int32 aircraftIdentifier;
		sf::Int32 action;
		bool actionEnabled;
		packet >> commanderIdentifier >> action >> actionEnabled;

		auto itr = mPlayers.find(commanderIdentifier);
		//auto itr = mPlayers.find(aircraftIdentifier);
		if (itr != mPlayers.end())
			itr->second->handleNetworkRealtimeChange(static_cast<Player::Action>(action), actionEnabled);
	} break;

	// Mission successfully completed
	case Server::MissionSuccess:
	{
		if (mGameStarted)
		{
			drawGameOverScreen();
			mGameStarted = false;
		}
	} break;

	// Pickup created
	case Server::SpawnPickup:
	{
		sf::Int32 type;
		sf::Vector2f position;
		packet >> type >> position.x >> position.y;

		mWorld.createPickup(position, static_cast<Pickup::Type>(type));
	} break;

	//
	case Server::UpdateClientState:
	{
		float currentWorldPosition;
		sf::Int32 commanderCount;
		//sf::Int32 aircraftCount;
		packet >> currentWorldPosition >> commanderCount;

		float currentViewPosition = mWorld.getViewBounds().top + mWorld.getViewBounds().height;

		// Set the world's scroll compensation according to whether the view is behind or too advanced
		mWorld.setWorldScrollCompensation(currentViewPosition / currentWorldPosition);

		for (sf::Int32 i = 0; i < commanderCount; ++i)
		{
			sf::Vector2f commanderPosition;
			sf::Int32 commanderIdentifier;
			packet >> commanderIdentifier >> commanderPosition.x >> commanderPosition.y;

			Commander* commander = mWorld.getCommander(commanderIdentifier);
			bool isLocalPlane = std::find(mLocalPlayerIdentifiers.begin(), mLocalPlayerIdentifiers.end(), commanderIdentifier) != mLocalPlayerIdentifiers.end();
			if (commander && !isLocalPlane)
			{
				sf::Vector2f interpolatedPosition = commander->getPosition() + (commanderPosition - commander->getPosition()) * 0.1f;
				commander->setPosition(interpolatedPosition);
			}
		}
	} break;
	}
}

void MultiplayerGameState::initialiseChatLog(Context context)
{
	// Set up the Message
	sf::Text message;
	message.setCharacterSize(20);
	message.setFont(context.fonts->get(Fonts::Main));

	for (int i = 0; i < 15; i++)
	{
		message.setString("");
		mChatMessages.push_back(message);
		mChatMessages[i].setPosition(25, 150 + (i * 25));

	}
	for (int i = 0; i < 10; i++)
	{
		mUsersLoggedOn.push_back(message);
		mUsersLoggedOn[i].setPosition(1000, 175 + (i * 25));
	}

}

std::string MultiplayerGameState::getName()
{
	std::ifstream inputFile("profile.txt");
	std::string name;
	if (inputFile >> name)
	{
		return name;
	}
}